# -*- coding: utf-8 -*-
"""
Created on Mon Oct 13 10:57:00 2014

@author: ep_work
"""


import xlrd
import numpy as np

def loadExpCompStandData(fpath,return_united_data=1):
  ExpData = []
  ExpSubjNums = []
  ExpProtNames = []
  ExpTimes = []
  rb = xlrd.open_workbook(fpath)

  for j in range(1,40):
    if(str(j) in rb.sheet_names()):
#      print('subj %i'%j)
      sheet = rb.sheet_by_name(str(j))
      ExpSubjNums.append(j)
      sData = []

      sProtList = sheet.col_values(0,1)

      sTimesList = sheet.row_values(0,1)

      for tj in range(len(sTimesList)):
        sData.append(sheet.col_values(tj+1,1))

      ExpTimes.append(sTimesList)
      ExpData.append(sData)
      ExpProtNames.append(sProtList)
  if(return_united_data==0):
    return (ExpSubjNums,ExpTimes,ExpProtNames,ExpData)

  uTimes = []
  [uTimes.extend(A) for A in ExpTimes]
  uTimes = np.unique(uTimes)

  uTD = {uTimes[j]:j for j in range(len(uTimes))}

  uProtNames = []
  [uProtNames.extend(A) for A in ExpProtNames]
  uProtNames = np.unique(uProtNames)

  uPND = {uProtNames[j]:j for j in range(len(uProtNames))}

  uData = np.zeros([len(ExpSubjNums),len(uTimes),len(uProtNames)])
  for sj in range(len(ExpSubjNums)):
    for tj in range(len(ExpTimes[sj])):
      for pj in range(len(ExpProtNames[sj])):
        uData[sj,uTD[ExpTimes[sj][tj]],uPND[ExpProtNames[sj][pj]]]=ExpData[sj][tj][pj]

  return (ExpSubjNums,uTimes,uProtNames,uData)

""" """
#

