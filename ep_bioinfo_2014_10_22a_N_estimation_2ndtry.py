# -*- coding: utf-8 -*-
"""
Created on Tue Oct 14 13:59:18 2014

@author: root
"""

#preliminary data loading:

import random

from ep_bioinfo_2014_10_13b_expComp_loadData import *

fpath = 'SFwork/SpaceFlight_1_10.xls'
fpath = 'SFwork/HB.xls'
ExpSubjNums,uTimes,uProtNames,uData=loadExpCompStandData(fpath,1)

S=shape(uData)
X = np.zeros([S[0]*S[1],S[2]])
for j in range(S[0]):
  for i in range(S[1]):
    X[j+i*S[0],:] = uData[j][i]

X = uData[:,0,:]
""" """

mK = mean(sum(X,axis=1))
sK = std(sum(X,axis=1))

M = shape(X)[0]

""" """
aaa
from ep_bioinfo_2014_10_08a_ps_of_prots import *

CNm=[]
CNs=[]

x=range(1,M+1)
for m in range(1,M+1):
  tCN=[]
  for rj in range(1000):
    tix=random.sample(range(M),m)
    tX = X[tix]
    tCN.append(sum( sum(tX,axis=0)>0 ))
  CNm.append(mean(tCN))
  CNs.append(std(tCN))

figure()
hold(True)
plot(x,CNm,'-.k')
for j in x:
  plot([j,j],[CNm[j-1]-CNs[j-1],CNm[j-1]+CNs[j-1]],'-sk')


def tryN(N,k=mK):
  figure()
  try:
    len(N)
  except:
    N=[N]
  hold(True)
  plot(x,CNm,'-.k')
  for n in N:
    plot(x,n*(1-(1-k/n)**x))
  for j in x:
    plot([j,j],[CNm[j-1]-CNs[j-1],CNm[j-1]+CNs[j-1]],'-sk')
  legend(['exp+-s']+map(lambda i:'N='+str(i),N),loc='upper left')
  xlabel('m people')
  ylabel('# of unique proteins')
  pass

def DoNK(N=[10,2000],k=[10,200],nsteps=[10,10]):

  N=linspace(N[0],N[1],nsteps[0])
  k=linspace(k[0],k[1],nsteps[1])



  img=np.zeros(nsteps)
  for j in range(nsteps[0]):
    for i in range(nsteps[1]):
      p=k[i]/N[j]
      p=min([1,p])
      y = N[j]*(1-(1-p)**x)
      immg[j][i]=log10(sqrt(mean((CNm-y)**2)))

  figure()
  plt.imshow(img,interpolation='nearest')
  plt.xticks(range(nsteps[1]),map(lambda f:str(int(f)),k),rotation=90)
  xlabel('k')
  plt.yticks(range(nsteps[0]),map(lambda f:str(int(f)),N),rotation=0)
  ylabel('N')

  colorbar()

def fNNfpfr(x,a,k=mK):
#  a=array([200,0,0])+array(a)*array([10000,500,1])
  N=a[0]
  Nf=a[1]
  pf=a[2]
  Nr=N-Nf
#  if(Nr<0):
#    Nr=0;
  pr=(k-pf*Nf)/Nr
  if(pf<pr):
    t=pf
    pf=pr
    pr=t
    t=Nf
    Nf=Nr
    Nr=t

#  if(pr<0):
#    pr=0
  return ([N,Nf,pf],Nf*(1.0-(1.0-pf)**array(x))+Nr*(1-(1.0-pr)**array(x)))


A0 =  [100,0,0]
A1 =  [400,200,1]
def fNNfpf(x,a,k=mK):
  a=array(A0)+array(a)*(array(A1)-array(A0))
  return fNNfpfr(x,a,k=k)


def tryNNfpf(Ps,k=mK):
  figure()
#  try:
#    len(N)
#  except:
#    N=[N]
  hold(True)
  plot(x,CNm,'-.k')
  RPs = []
  for a in Ps:
    rp,y = fNNfpf(x,a,k=k)
    plot(x,y)
    RPs.append(rp)
  for j in x:
    plot([j,j],[CNm[j-1]-CNs[j-1],CNm[j-1]+CNs[j-1]],'-sk')
  legend(['exp+-s']+map(lambda i:'N=%s Nf=%i pf=%.2f'%(i[0],i[1],i[2]),RPs),loc='upper left')
  xlabel('m people')
  ylabel('# of unique proteins')
  pass

def tryNNfpfr(Ps,k=mK):
  figure()
#  try:
#    len(N)
#  except:
#    N=[N]
  hold(True)
  plot(x,CNm,'-.k')
  RPs = []
  for a in Ps:
    rp,y = fNNfpfr(x,a,k=k)
    plot(x,y)
    RPs.append(rp)
  for j in x:
    plot([j,j],[CNm[j-1]-CNs[j-1],CNm[j-1]+CNs[j-1]],'-sk')
  legend(['exp+-s']+map(lambda i:'N=%s Nf=%i pf=%.2f'%(i[0],i[1],i[2]),RPs),loc='upper left')
  xlabel('m people')
  ylabel('# of unique proteins')
  pass


def NNfpfR(a):
  y=(CNm - fNNfpf(x,a)[1])
  return sqrt(mean(((y[0:-1]**2)/CNs[0:-1])))

def NNfpfrR(a):
  y=(CNm - fNNfpfr(x,a)[1])
  return sqrt(mean(((y[0:-1]**2)/CNs[0:-1])))

def findRand():
  minR=10000
  mina=[0,0,0]
  while(1):
    a = np.random.rand(3)
    R=NNfpfR(a)
    if(R<minR):
      minR=R
      mina=a
      print(mina)
      print('R=%.2f'%R)
def findRandr(A0 = [100,0,0],A1 = [400,200,1]):
  minR=10000
  mina=[0,0,0]
  while(1):
    a = np.random.rand(3)
    a=array(A0)+array(a)*(array(A1)-array(A0))
    R=NNfpfrR(a)
    if(R<minR):
      minR=R
      mina=a
      print('['+','.join(map(str,mina))+']')
      print('R=%.2f'%R)

def DoNNf(Ns=[200,2000],Nfs=[100,1000],pf=0.5,nsteps=[10,10]):
  Ns=linspace(Ns[0],Ns[1],nsteps[0])
  Nfs=linspace(Nfs[0],Nfs[1],nsteps[1])


  img=np.zeros(nsteps)
  for j in range(nsteps[0]):
    for i in range(nsteps[1]):
      img[j][i]=log10(NNfpfrR([Ns[j],Nfs[i],pf]))

  figure()
  plt.imshow(img,interpolation='nearest')
  plt.yticks(range(nsteps[0]),map(str,Ns))
  ylabel('N')
  plt.xticks(range(nsteps[1]),map(lambda f:str(int(f)),Nfs))
  xlabel('Nf')

  colorbar()

def DoNfpf(Nfs=[0,200],pfs=[0.1,1],N=2000,nsteps=[10,10]):
  Nfs=linspace(Nfs[0],Nfs[1],nsteps[0])
  pfs=linspace(pfs[0],pfs[1],nsteps[1])


  img=np.zeros(nsteps)
  for j in range(nsteps[0]):
    for i in range(nsteps[1]):
      img[j][i]=log10(NNfpfrR([N,Nfs[j],pfs[i]]))

  figure()
  plt.imshow(img,interpolation='nearest')
  plt.xticks(range(nsteps[1]),map(lambda f:'%.2f'%f,pfs),rotation=90)
  xlabel('pf')
  plt.yticks(range(nsteps[0]),map(lambda f:str(int(f)),Nfs),rotation=0)
  ylabel('Nf')

  colorbar()

def DomNfNf(mNfs=[0,150],Nfs=[200,2000],N=2000,nsteps=[10,10]):
  Nfs=linspace(Nfs[0],Nfs[1],nsteps[1])
  mNfs=linspace(mNfs[0],mNfs[1],nsteps[0])


  img=np.zeros(nsteps)
  for j in range(nsteps[0]):
    for i in range(nsteps[1]):
      img[j][i]=log10(NNfpfrR([N,Nfs[i],mNfs[j]/Nfs[i]]))

  figure()
  plt.imshow(img,interpolation='nearest')
  plt.xticks(range(nsteps[1]),map(lambda f:str(int(f)),Nfs),rotation=90)
  xlabel('Nf')
  plt.yticks(range(nsteps[0]),map(lambda f:str(int(f)),mNfs),rotation=0)
  ylabel('mNf')

  colorbar()


def DoNfkf(Nfs=[0,200],pfs=[0.1,1],N=2000,nsteps=[10,10]):
  Nfs=linspace(Nfs[0],Nfs[1],nsteps[0])
  pfs=linspace(pfs[0],pfs[1],nsteps[1])


  img=np.zeros(nsteps)
  for j in range(nsteps[0]):
    for i in range(nsteps[1]):
      img[j][i]=log10(NNfpfrR([N,Nfs[j],pfs[i]]))

  figure()
  plt.imshow(img,interpolation='nearest')
  plt.xticks(range(nsteps[1]),map(lambda f:'%.2f'%f,pfs),rotation=90)
  xlabel('pf')
  plt.yticks(range(nsteps[0]),map(lambda f:str(int(f)),Nfs),rotation=0)
  ylabel('Nf')

  colorbar()


def tryNk(N):
  Rs = []
  for n in N:
    Rs.append(sqrt(mean(((CNm[0:-1]-n*(1-(1-mK/n)**x[0:-1]))/CNs[0:-1])**2)))
  figure()
  plot(N,Rs)
  xlabel('N')
  ylabel('RMS')
  pass
""" """
#