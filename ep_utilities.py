# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 11:23:29 2014

@author: ep_work
"""

print 'ep_utilities!'

import numpy as np
from numpy import *
from numpy import array
from numpy.random import *
from matplotlib.mlab import *
from scipy.stats.stats import pearsonr
from matplotlib.pyplot import hist,plot
import gzip
import pickle
import copy
import six

import networkx as nx

#import wx
import re
import time


def ep_rplot(a,b,fmt,randAddC=0):
  plot(a+randAddC*np.random.randn(len(a)),b+randAddC*np.random.randn(len(a)),fmt)


import os




def ep_curdir():
  return os.path.dirname(os.path.abspath(__file__))
def ep_allfilesin(path):
  return [ f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f)) ]

def fullfile(path,f):
  return os.path.join(path,f)

def nnan(x):
  return x[~isnan(x)]

def unique_Arrays(AA,return_index=0,return_inverse=0):
  if(len(AA)==0):
    return []
  tsep = '__'
  s = [tsep.join(map(str,A)) for A in AA]
#  print(s)
  if(return_index or return_inverse):
    R = unique(s,return_index=return_index,return_inverse=return_inverse)
    us=R[0]
#    print('aa')
  else:
    us = unique(s)
    R=()
  uA = array([map(int,re.findall(tsep.join(['([0-9]+)']*len(AA[0])),t)[0]) for t in us])


  if(return_index or return_inverse):
    return (uA,)+R[1:]
  else:
    return uA
def pickleSave(Obj,path):
  with open(path,'wb') as f:
    pickle.dump(Obj,f,-1)

def pickleLoad(path):
  with open(path,'rb') as f:
    R = pickle.load(f)
  return R

def get_path(wildcard):
    app = wx.App(None)
    style = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
    dialog = wx.FileDialog(None, 'Open', wildcard=wildcard, style=style)
    if dialog.ShowModal() == wx.ID_OK:
        path = dialog.GetPath()
    else:
        path = None
    dialog.Destroy()
    return path


def isString(s):
    return  isinstance(s, six.string_types)

def R2(y,p):
    my=mean(y)
    return 1- (sum((y-p)**2))/(sum((y-my)**2))

def readCSV(fname,header=0):
    csvfile = open( fname, "r" )
    dialect = csv.Sniffer().sniff( csvfile.read( 100*1024 ) )
    csvfile.seek(0)
    # read csv file according to dialect
    reader = csv.reader( csvfile, dialect )
    if(header):
        # read header
        header = reader.next()
    # read data into memory
    R = [row for row in reader]
    # close input file
    csvfile.close()
    return R

def comLen(list1, list2):
    return len(set(list1) & set(list2))

def common_elements(list1, list2=None):
    if(list2==None):
        return list(set.intersection(*map(set,list1)))
    else:
        return list(set(list1) & set(list2))

def findFirst(X):
    return next((i for i, x in enumerate(X) if x), -1)

def ep_hist(X,n=50):
    return hist(array(X)[~isnan(X)].flatten(),n)


def ep_YBadj(op):
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();
    nv = (p[six]*len(p)*1.0/(1.0+np.arange(len(p)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,0,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    return nv.reshape(s) #reshaping into original array and returning

def ep_YBadj2(op):
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();

    realLength=findFirst(isnan(p[six]))
    if(realLength<0):
        realLength = len(p)

    nv = (p[six]*realLength*1.0/(1.0+array(range(realLength)+[NaN]*(len(p)-realLength)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,0,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    return nv.reshape(s) #reshaping into original array and returning

def pFDRadj(op):
    import numpy as np
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = np.shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();

    realLength=next((i for i, x in enumerate(np.isnan(p[six])) if x), -1 ) #find First
    if(realLength<0):
        realLength = len(p)

    nv = (p[six]*realLength*1.0/(1.0+array(range(realLength)+[NaN]*(len(p)-realLength)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,-1,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    nv[nv>1]=1;
    return nv.reshape(s) #reshaping into original array and returning

def ep_BHcorr2(op):
    # changed to YBadj simply not to rewrite all the code that used this function wrong
    return ep_YBadj2(op)
"""
# old stuff
    s = shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();
    return (p[six]*len(p)*1.0/(1.0+np.arange(len(p))))[six.argsort()].reshape(s)
"""
def ep_BHcorr2_old(op):
    s = shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();
    return (p[six]*len(p)*1.0/(1.0+np.arange(len(p))))[six.argsort()].reshape(s)



def tablePrint(X,formatString='%.2f'):
  for Ds in X:
    print('\t'.join([formatString % d for d in Ds]))
def tablePrintF(formatString,X):
  for Ds in X:
    print('\t'.join([formatString % d for d in Ds]))

def corrcoefNNAN(A,B):
  temp = (~isnan(A))&(~isnan(B))
  if(sum(temp)>5):
    return corrcoef(A[temp],B[temp])[0,1]
  else:
    return nan

def finitePearsonCorrP(A,B,corr=0,distMx=0,sameStuff=1,minNNaNs=1,verbose=1):
  if(shape(A)[1] != shape(B)[1]):
    raise Exception("size(A)[1] != size(B)[1]")

  R = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  RP = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  if(distMx):
    Rdm = [];
    RPdm = [];

#  print('aa')
  allNans=0
  for j in range(shape(A)[0]):
    for i in range(shape(B)[0]):
#      print(str(j)+'i='+str(i))
      if(sameStuff)and(j==i):
        R[j,i]=1
        RP[j,i]=0
        continue;
#      print('bb')
      temp = (~isnan(A[j,:]))&(~isnan(B[i,:]))
      if(sum(temp)<minNNaNs):
          allNans=allNans+1
          R[j,i]  = NaN
          RP[j,i] = NaN
      else:
          R[j,i],RP[j,i] = pearsonr(A[j,temp],B[i,temp])
          if(corr*(j-i)!=0):
            if(R[j,i]*corr>0):
              RP[j,i]=RP[j,i]/2
            else:
              RP[j,i]=1-(RP[j,i]/2)
      if(distMx and (i>j)):
          Rdm.append(R[j,i])
          RPdm.append(RP[j,i])
  if(verbose):
      print('all NaNs in '+str(allNans)+'/'+str(shape(A)[0]*shape(B)[0]))
  if(distMx):
    return R,RP,Rdm,RPdm
  else:
    return R,RP



####################################################################################
######## Binomial stuff
####################################################################################


#with gzip.GzipFile('binomialCovDist.pickle.gz','w') as f:
#    pass
def getBinomialCovDist(N):
   try:
       return binomialCovDist[N]
   except:
       try:
           with gzip.GzipFile('binomialCovDist.pickle.gz','r') as f:
               binomialCovDist=pickle.load(f)
           return binomialCovDist[N]
       except:
           try:
               binomialCovDist
           except:
               binomialCovDist={}
           binomialCovDist[N]={}
           A=randn(3000000.0/N,N,2)>=0
           t=(sum(A[:,:,0]*A[:,:,1],axis=1)*shape(A)[1]-sum(A[:,:,1],axis=1)*sum(A[:,:,0],axis=1))
           binomialCovDist[N]['range']=range(min(t),max(t)+1)
           binomialCovDist[N]['cdf']  =array([sum(t<X) for X in binomialCovDist[N]['range']])*1.0/len(t)
           with gzip.GzipFile('binomialCovDist.pickle.gz','w') as f:
               pickle.dump(binomialCovDist,f)
           return binomialCovDist[N]


def binaryCovP(A,B,nR=1000):
    L = len(A)
    if(L!=len(B)):
        raise Exception("lengths do't match!")



    temp = isnan(A)|isnan(B)
    if(sum(temp)>0):

       nA = array(A)[~temp]
       nB = array(B)[~temp]

       BCD = getBinomialCovDist(L)
       Cov = L*sum(array(nA)*nB)-sum(nA)*sum(nB)
       Cov = Cov - sum(temp)*(sum(nA)+sum(nB))*1.0/2  # additive from average of covariance of at-least-one-side-NaN vectors
       P = BCD['cdf'][find(BCD['range']>=-abs(Cov))[0]]
       Cov=Cov*1.0/(L**2)
       return Cov,P
       # NaN->Random
       L = len(A)
       BCD = getBinomialCovDist(L)
       tA = array(tile(A,[nR*(2**sum(temp)),1]))
       tB = array(tile(B,[nR*(2**sum(temp)),1]))
       tA[isnan(tA)] = rand(len(tA[isnan(tA)]))>0.5
       tB[isnan(tB)] = rand(len(tB[isnan(tB)]))>0.5
       tA = array(tA)
       tB = array(tB)
       Cov = mean(L*sum(tA*tB,axis=1)-sum(tA,axis=1)*sum(tB,axis=1))
       P = BCD['cdf'][find(BCD['range']>=-abs(Cov))[0]]
       Cov=Cov*1.0/(L**2)

    else: # no NaNs o_O
       BCD = getBinomialCovDist(L)
       Cov = L*sum(array(A)*B)-sum(A)*sum(B)
       P = BCD['cdf'][find(BCD['range']==-abs(Cov))[0]]
       Cov=Cov*1.0/(L**2)

    return Cov,P

#a=[]
#[a.extend(binaryCovP([0,0,0,0,0,0,0,0,0,1,0,0,0,NaN,NaN],[0,0,0,0,0,0,0,1,0,0,0,NaN,NaN,0,0])) for j in range(1000)]


def ep_spec_corrcoef(A,B):
    A=np.array(A)
    B=np.array(B)
    if(shape(A)[0]!=shape(B)[0]):
        raise ShapesDontMatch
    laxis=0
    if(len(shape(A))>1):
        laxis=1
        if(shape(A)[1]!=shape(B)[1]):
            raise ShapesDontMatch
    L = shape(A)[laxis]
#    R = (L*1.0*np.sum(A*B,axis=laxis) - np.sum(A,axis=laxis)*np.sum(B,axis=laxis))*1.0/(L**2)
    R=1
    R = R/(np.std(A,axis=laxis)*np.std(B,axis=laxis))
    return R

def getBinomialCorrDist(N):
   try:
       return binomialCorrDist[N]
   except:
       try:
           with gzip.GzipFile('binomialCorrDist.pickle.gz','r') as f:
               binomialCorrDist=pickle.load(f)
           return binomialCorrDist[N]
       except:
           try:
               binomialCorrDist
           except:
               binomialCorrDist={}
           binomialCorrDist[N]={}
           A=randn(3000000.0/N,N,2)>=0
           t=ep_spec_corrcoef(A[:,:,0],A[:,:,1])
           ut=unique(t)
           ut=ut[~isnan(ut)]
           binomialCorrDist[N]['range']=ut
           binomialCorrDist[N]['cdf']  =array([sum(t<X) for X in binomialCorrDist[N]['range']])*1.0/len(t)
           with gzip.GzipFile('binomialCorrDist.pickle.gz','w') as f:
               pickle.dump(binomialCorrDist,f)
           return binomialCorrDist[N]

def binaryCorrP(A,B,nR=1000):
    L = len(A)
    if(L!=len(B)):
        raise Exception("lengths do't match!")



    temp = isnan(A)|isnan(B)
    if(sum(temp)>0):

       nA = array(A)[~temp]
       nB = array(B)[~temp]

#       BCD = getBinomialCorrDist(L)
#       Cov = L*sum(array(nA)*nB)-sum(nA)*sum(nB)
#       Cov = Cov - sum(temp)*(sum(nA)+sum(nB))*1.0/2  # additive from average of covariance of at-least-one-side-NaN vectors
#       P = BCD['cdf'][find(BCD['range']>=-abs(Cov))[0]]
#       Cov=Cov*1.0/(L**2)
#       return Cov,P
       # NaN->Random
       L = len(A)
       BCD = getBinomialCorrDist(L)
       tA = array(tile(A,[nR*(2**sum(temp)),1]))
       tB = array(tile(B,[nR*(2**sum(temp)),1]))
       tA[isnan(tA)] = rand(len(tA[isnan(tA)]))>0.5
       tB[isnan(tB)] = rand(len(tB[isnan(tB)]))>0.5

       Corr = ep_spec_corrcoef(tA,tB)
       Corr = mean(Corr)
       P = BCD['cdf'][find(BCD['range']>=-abs(Corr))[0]]

    else: # no NaNs o_O
       BCD = getBinomialCorrDist(L)
       Corr = ep_spec_corrcoef(A,B)
       P = BCD['cdf'][find(BCD['range']==-abs(Corr))[0]]

    return Corr,P

def binaryCovTableP(A,B,corr=0,distMx=0,sameStuff=1,minNNaNs=1):
  if(shape(A)[1] != shape(B)[1]):
    raise Exception("size(A)[1] != size(B)[1]")

  R = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  RP = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  if(distMx):
    Rdm = [];
    RPdm = [];

#  print('aa')
  allNans=0
  for j in range(shape(A)[0]):
    for i in range(shape(B)[0]):

#      print(str(j)+'i='+str(i))
      if(sameStuff)and(j==i):
        R[j,i]=1
        RP[j,i]=0
        continue;
#      print('bb')
      temp = (~isnan(A[j,:]))&(~isnan(B[i,:]))
      if(sum(temp)<minNNaNs):
          allNans=allNans+1
          R[j,i]  = NaN
          RP[j,i] = NaN
      else:
          R[j,i],RP[j,i] = binaryCovP(A[j,:],B[i,:])
          if(corr*(j-i)!=0):
            if(R[j,i]*corr>0):
              RP[j,i]=RP[j,i]/2
            else:
              RP[j,i]=1-(RP[j,i]/2)
      if(distMx and (i>j)):
          Rdm.append(R[j,i])
          RPdm.append(RP[j,i])

  print('all NaNs in '+str(allNans)+'/'+str(shape(A)[0]*shape(B)[0]))
  if(distMx):
    return R,RP,Rdm,RPdm
  else:
    return R,RP


def ep_corrP1(a,b,nrt=1000):
#    temp = (~isnan(a)&~isnan(b))
#    a=array(a)[temp]
#    b=array(b)[temp]
    ABC = ep_spec_corrcoef(a,b)


    ta = np.array([list(a)]*nrt)
    tb = np.zeros(shape=(nrt,len(a)))
    for j in range(nrt):
        np.random.shuffle(b)
        tb[j,:] = copy.copy(b)
    c = ep_spec_corrcoef(ta,tb)
    p = sum(abs(c)>abs(ABC))*1.0/len(c)

    if(p<.15):
        coeff=9;
        ta = np.array([list(a)]*nrt*coeff)
        tb = np.zeros(shape=(nrt*coeff,len(a)))
        for j in range(nrt*coeff):
            np.random.shuffle(b)
            tb[j,:] = copy.copy(b)
        c=c.tolist()
        c.extend(ep_spec_corrcoef(ta,tb))
        c=array(c)
        p = sum(abs(c)>abs(ABC))*1.0/len(c)

    return ABC,p


# produce correlation matrix between vectors from A and B (all pairs thereof)
#   p-values are obtained using permutation method (see ep_corrP1 pocedure)
#
def finitePearsonEPCorrP(A,B,corr=0,distMx=0,sameStuff=1,minNNaNs=1,verbose=0,nrt=1000):
  if(shape(A)[1] != shape(B)[1]):
    raise Exception("size(A)[1] != size(B)[1]")

  R = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  RP = np.zeros(shape=(shape(A)[0],shape(B)[0]))
  if(distMx):
    Rdm = [];
    RPdm = [];

#  print('aa')
  allNans=0
  for j in range(shape(A)[0]):
    for i in range(shape(B)[0]):
#      print(str(j)+'i='+str(i))
      if(sameStuff)and(j==i):
        R[j,i]=1
        RP[j,i]=0
        continue;
      if(sameStuff)and(i>j):
        R[j,i]=R[i,j]
        RP[j,i]=RP[i,j]
        continue;
#      print('bb')
      temp = (~isnan(A[j,:]))&(~isnan(B[i,:]))
      if(sum(temp)<minNNaNs):
          allNans=allNans+1
          R[j,i]  = NaN
          RP[j,i] = NaN
      else:
          R[j,i],RP[j,i] = ep_corrP1(A[j,temp],B[i,temp],nrt=nrt)
          if(corr*(j-i)!=0):
            if(R[j,i]*corr>0):
              RP[j,i]=RP[j,i]/2
            else:
              RP[j,i]=1-(RP[j,i]/2)
      if(distMx and (i>j)):
          Rdm.append(R[j,i])
          RPdm.append(RP[j,i])
    if(verbose>0):
        print(str(j)+'/'+str(shape(A)[0]))
  if(allNans>0):
      print('all NaNs in '+str(allNans)+'/'+str(shape(A)[0]*shape(B)[0]))
  if(distMx):
    return R,RP,Rdm,RPdm
  else:
    return R,RP



def gmean(X):
    return (prod(1.0*(X+1))**(1.0/len(X)))

def meanNoNaNs(X,axis=0):
    temp = array(X)
    tempNNaNsum = sum(~isnan(temp),axis=axis)
    temp[isnan(temp)]=0
    return sum(temp,axis=axis)/tempNNaNsum

from scipy.stats import fisher_exact


# fisher's exact test designed for BinGO output
#
#     Arguments are received as follows:
#      -  Number of genes in lit with this annotation,
#      -  total amount of genes with this annotation,
#      -  total amounts of genes in the list,
#      -  total amount of genes in the whole of GO (14306)
def ep_GOfisher_exact(a,b,c,d=14306):
    temp,p = fisher_exact([[a,b-a],[c-a,d-b-c+a]],alternative='greater')
    return p


def ep_isString(s):
    return ((type(s)==type('aa')    ) or(type(s)==type(u'aa')) or (type(s)==type(array('aa'))))


def ep_imagesc(X,t=0):
    tX = X;
    if(t):
        tX = array(tX).transpose()
    inter = 'nearest'
    plt.imshow(tX,interpolation = inter)

import matplotlib.pyplot as plt
cmap =  plt.cm.seismic;
def ep_imshow(X,interpolation='nearest',cmap=cmap,bipolar=nan,vmin=0,vmax=1,useV=0,showCB=1):
    if(isnan(bipolar)):
        bipolar = min(X.flatten())<0
    if(useV==0):
        if(bipolar==1):
            vmin = -max(abs(X.flatten()));
            vmax = max(abs(X.flatten()));
            plt.imshow(X,interpolation='nearest',cmap=cmap,vmin = vmin,vmax=vmax)
        else:
            vmin = min(X.flatten());
            vmax = max(X.flatten());
            plt.imshow(X,interpolation='nearest',vmin = vmin,vmax=vmax)
    if(bipolar==1):
        plt.imshow(X,interpolation='nearest',cmap=cmap,vmin = vmin,vmax=vmax)
    else:
        plt.imshow(X,interpolation='nearest',vmin = vmin,vmax=vmax)
    if(showCB):
        colorbar()


def interSubjectPearsonR(X,corr=0,distMx=0):
  R = np.zeros(shape=(shape(X)[1],shape(X)[0],shape(X)[0]))
  RP = R.copy()
  if(distMx):
    Rdm=[]
    RPdm=[]
    for j in range(shape(X)[1]):
      Rdm.append([])
      RPdm.append([])
      R[j,:,:],RP[j,:,:],Rdm[j],RPdm[j] = finitePearsonCorrP(X[:,j,:],X[:,j,:],corr=corr,distMx = distMx,sameStuff=1,verbose=0);

    return R,RP,array(Rdm),array(RPdm)
  else:
    for j in range(shape(X)[1]):
      R[j,:,:],RP[j,:,:] = finitePearsonCorrP(X[:,j,:],X[:,j,:],corr=corr,sameStuff=1,verbose=0);
    return R,RP
