# -*- coding: utf-8 -*-
"""
Created on Wed Nov 19 12:52:41 2014

tries to implement a distance approach
  - to find proteins that have shown similar profiles in bum,bup and sfm
    but a significantly different profile in sfp7 and sfp1 somewhere in between

@author: ep_work
"""
from ep_utilities import ep_rplot


def load(fname):
  rb = xlrd.open_workbook('SFwork/'+fname+'.xls')

  # sf 10subj =>



  ExpData = []
  ExpProtNames = []
  ExpSubjNums = []
  ExpTimes = []

  try:
    subj_list
    subj_listS = 'subjs'+''.join([str(j+1) for j in subj_list])
  except:
    subj_list = range(len(rb.sheet_names()))
    subj_listS=''

  for j in subj_list:
      if(str(j+1) in rb.sheet_names()):
  #        trint('subj %i'%(j+1))
          ExpSubjNums.append(j+1)
          sheet = rb.sheet_by_name(str(j+1))

          ExpTimes.append(sheet.row_values(0,1))
          ExpProtNames.append(sheet.col_values(0,1))

          tData=[]
          for i in range(len(ExpTimes[-1])):
              tData.append(sheet.col_values(i+1,1))
          ExpData.append(tData)

  try:
    upn
    uProtNames = upn
    usedAll=1
  except:
    uProtNames = []
    for A in ExpProtNames:
        for s in A:
            if(s not in uProtNames):
                uProtNames.append(s)

    uProtNames.sort()
    usedAll=0

  uPND = {}
  for j in range(len(uProtNames)):
      uPND[uProtNames[j]]=j

  uNP = len(uProtNames)


  uData = []
  for sj in range(len(ExpSubjNums)):
      sData = []
      for tj in range(len(ExpTimes[sj])):
          sData.append([0]*uNP)
          for pj in range(len(ExpProtNames[sj])):
              sData[-1][uPND[ExpProtNames[sj][pj]]] = ExpData[sj][tj][pj]
      uData.append(sData)

  return uProtNames,uData

uPN0,X0 = load( 'backup_men_1_6')
uPN1,X1 = load( 'SpaceFlight_1_10')


""" unite! """

# combined unique list
uPN = uPN0
uPN.extend(uPN1)
uPN = unique(uPN)

# combined data

def redoX(X,oPN,nPN):
  tX = np.swapaxes(X,0,1)
  tX = np.swapaxes(tX,2,1)
  nX = np.zeros((shape(tX)[0],len(nPN),shape(tX)[2]))

  for nj in range(len(nPN)):
    try:
      for j in range(shape(tX)[0]):
        nX[j,nj,:] = tX[j,oPN.index(nPN[nj]),:]
    except:
      pass


  return nX

uX1 = redoX(X1,uPN1,uPN)
uX0 = redoX(X0,uPN0,uPN)

uD = []
for j in range(shape(uX0)[0]):
  uD.append(uX0[j,:,:])
for j in range(shape(uX1)[0]):
  uD.append(uX1[j,:,:])

# and .. differences?

# bu1-bu0 cf1-bu0


#simple difs and complicated difs
#   simple are sqrs of difs of mean
#   complicated are (if possible) mean sqrs of difs

#since two different experiments comtain different people
#  - we cannot do anything more than just comparison of mean
#     if we compare timepoints from different experiments.
sdifs = np.zeros((shape(uD)[0],shape(uD)[0],len(uPN)))
cdifs = np.zeros((shape(uD)[0],shape(uD)[0],len(uPN)))
for j1 in range(shape(uD)[0]):
  for j2 in range(j1,shape(uD)[0]):
#    print(shape(uD[j1])[1])
#    print(shape(uD[j2])[1])
    sdifs[j1,j2] = (mean(uD[j1],axis=1) - mean(uD[j2],axis=1))**2
    if(shape(uD[j1])[1] == shape(uD[j2])[1]):
      cdifs[j1,j2] = mean((uD[j1]-uD[j2])**2,axis=1)
    else:
      cdifs[j1,j2] = sdifs[j1,j2]
    sdifs[j2,j1] = sdifs[j1,j2]
    cdifs[j2,j1] = cdifs[j1,j2]

cdifs=np.swapaxes(cdifs,0,2)
sdifs=np.swapaxes(sdifs,0,2)

#

#ep_rplot(cdifs[:,0,2],cdifs[:,1,2],'.',0.002)

M = cdifs

t = 1*M[:,0,1]+M[:,0,2]+1*M[:,1,2]+3-M[:,2,3]-M[:,2,4]-M[:,3,4]-M[:,1,3]-M[:,0,3]

tix=argsort(t)



from xlsxwriter.workbook import Workbook

wb=Workbook("SFwork/raw/ep_Tiys0_buSF_distance.xlsx")

times=['bum','bup','sfm','sfp1','sfp7']



s = wb.add_worksheet('cdifs')
s.write(0,0,'ProtNames\difs')
tn=0
for j1 in range(shape(uD)[0]):
  for j2 in range(j1+1,shape(uD)[0]):
    tn=tn+1
    s.write(0,tn,times[j1]+'-'+times[j2])
for pj in range(len(uPN)):
  s.write(pj+1,0,uPN[pj])
  tn=0
  for j1 in range(shape(uD)[0]):
    for j2 in range(j1+1,shape(uD)[0]):
      tn=tn+1
      s.write(pj+1,tn,cdifs[pj,j1,j2])
s.conditional_format(1,1,len(uPN)+1,tn, {'type': '2_color_scale',
                               'min_color': "#FFFFFF",
                               'max_color': "#00FF00",
                                })
s = wb.add_worksheet('sdifs')
s.write(0,0,'ProtNames\difs')
tn=0
for j1 in range(shape(uD)[0]):
  for j2 in range(j1+1,shape(uD)[0]):
    tn=tn+1
    s.write(0,tn,times[j1]+'-'+times[j2])
for pj in range(len(uPN)):
  s.write(pj+1,0,uPN[pj])
  tn=0
  for j1 in range(shape(uD)[0]):
    for j2 in range(j1+1,shape(uD)[0]):
      tn=tn+1
      s.write(pj+1,tn,sdifs[pj,j1,j2])
s.conditional_format(1,1,len(uPN)+1,tn, {'type': '2_color_scale',
                               'min_color': "#FFFFFF",
                               'max_color': "#00FF00",
                                })
#for j in tix:
#  print('%.4f\t%s'%(t[j],uPN[j]))
s = wb.add_worksheet('smartC')
s.write(0,0,'ProtNames')
s.write(0,1,'smart coef')
for i in range(len(tix)):
  j=tix[i]
  s.write(i+1,0,uPN[i])
  s.write(i+1,1,t[i])
s.conditional_format(1,1,len(uPN)+1,1, {'type': '2_color_scale',
                               'max_color': "#FFFFFF",
                               'min_color': "#00FF00",
                                })
""" """
#

