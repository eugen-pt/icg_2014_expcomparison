import xlrd
from time import clock

tStart = clock()


def trint(s):
  print('%5.2f\t%s'%(clock()-tStart,s))

nMax=26
nMax=24
#nMax=20
#nMax=15

bu_fname= 'backup_men_1_6'
bu_ds = [[1,0],[0,1],[1,1],[-1,1]]
sf_fname= 'SpaceFlight_1_10'
sf_ds = [[1,0,0],[0,1,0],[0,0,1],[1,1,1],[-1,1,0],[-1,0,1],[0,-1,1],[-1,2,-1],[-2,1,1],[1,1,-2]]

fnames = [bu_fname,sf_fname,sf_fname,sf_fname]
dss    = [bu_ds,sf_ds,sf_ds,sf_ds]
subj_lists = [range(6),range(10),range(6),range(6,10)]

from ep_bioinfo_2014_12_01b_N_swissprot import *
from ep_bioinfo_2014_12_01a_N_of_all_Exps import *

NTotalNames = ['AllHere','AllExps_butM105' ,'AllExps' ,'AllSwissProt']
NTotals     = [-153     ,allExps_butM105_NP,allExps_NP,SwissProt_NP ]

#fnames = [fnames[0]]

for FileJ in range(len(fnames)):
  fname =  fnames[FileJ]
  print(fname)
  ds=dss[FileJ]
  subj_list=subj_lists[FileJ]

  trint(fname)
  rb = xlrd.open_workbook('SFwork/'+fname+'.xls')

  # sf 10subj =>



  ExpData = []
  ExpProtNames = []
  ExpSubjNums = []
  ExpTimes = []

  try:
    subj_list
    subj_listS = 'subjs'+''.join([str(j+1) for j in subj_list])
  except:
    subj_list = range(len(rb.sheet_names()))
    subj_listS=''

  for j in subj_list:
      if(str(j+1) in rb.sheet_names()):
  #        trint('subj %i'%(j+1))
          ExpSubjNums.append(j+1)
          sheet = rb.sheet_by_name(str(j+1))

          ExpTimes.append(sheet.row_values(0,1))
          ExpProtNames.append(sheet.col_values(0,1))

          tData=[]
          for i in range(len(ExpTimes[-1])):
              tData.append(sheet.col_values(i+1,1))
          ExpData.append(tData)

  try:
    upn
    uProtNames = upn
    usedAll=1
  except:
    uProtNames = []
    for A in ExpProtNames:
        for s in A:
            if(s not in uProtNames):
                uProtNames.append(s)

    uProtNames.sort()
    usedAll=0

  uPND = {}
  for j in range(len(uProtNames)):
      uPND[uProtNames[j]]=j

  uNP = len(uProtNames)


  uData = []
  for sj in range(len(ExpSubjNums)):
      sData = []
      for tj in range(len(ExpTimes[sj])):
          sData.append([0]*uNP)
          for pj in range(len(ExpProtNames[sj])):
              sData[-1][uPND[ExpProtNames[sj][pj]]] = ExpData[sj][tj][pj]
      uData.append(sData)

  trint('loaded')

  X = np.swapaxes(uData,0,1)

  #aaa

  resData = []
  rescData = []
  meanData = mean(uData,axis=0)

  from ep_bioinfo_2014_11_13a_ps_of_prots_dyns import *


  for Nj in range(len(NTotals)):
    NTotal= NTotals[Nj]

    rdsS = []
    rds = []
    Ps = []
    Psc = []

    MultiD = 0 # 1 is about 2 times slower =(

    if(MultiD==0):
      for dj in range(len(ds)):
        trint('f %i/%i N %i/%i dyn %i/%i'%(FileJ,len(fnames),Nj,len(NTotals),dj,len(ds)))
        d=ds[dj]
        t = getDynPs(X,d,nMax=nMax,NTotal=NTotal)
        rds.append(d)
        rdsS.append('['+','.join(map(str,d))+']')
        Ps.append(t[0][0])
        Psc.append(t[1][0])
        td = list(1.0*array(d))
        for tdj in range(len(td)):
          if(td[tdj]!=0):
            td[tdj]=-td[tdj]
#        t = getDynPs(X,td,nMax=nMax)
        rds.append(td)
        rdsS.append('['+','.join(map(str,td))+']')
        Ps.append(t[0][1])
        Psc.append(t[1][1])
    else:
      trint('f %i/%i N %i/%i all dyns'%(FileJ,len(fnames),Nj,len(NTotals)))
      p,pc =getDynPs(X,ds,nMax=nMax,NTotal=NTotal)
      for dj in range(len(ds)):
        d=ds[dj]
        rds.append(d)
        rdsS.append('['+','.join(map(str,d))+']')
        Ps.append(p[dj][0])
        Psc.append(pc[dj][0])
        td = list(1.0*array(d))
        for tdj in range(len(td)):
          if(td[tdj]!=0):
            td[tdj]=-td[tdj]
#        t = getDynPs(X,td,nMax=nMax)
        rds.append(td)
        rdsS.append('['+','.join(map(str,td))+']')
        Ps.append(p[dj][1])
        Psc.append(pc[dj][1])


    p,pc = getNConstPs(X,nMax=nMax,NTotal=NTotal)
    rdsS.append('const')
    Ps.append(p[1])
    Psc.append(pc[1])
    rdsS.append('Nconst')
    Ps.append(p[0])
    Psc.append(pc[0])


    from xlsxwriter.workbook import Workbook
    wb=Workbook("SFwork/raw/ep_StatSign_Dynv1_"+str(usedAll)+"_"+fname+"_"+subj_listS+"_Neq-"+NTotalNames[Nj]+"_nM%i"%nMax+".xlsx")


    red_format = wb.add_format({'bg_color':   '#FFC7CE',
                                'font_color': '#9C0006'})
    green_format = wb.add_format({'bg_color':   '#C7FFCE',
                                  'font_color': '#009C06'})
    def save(X,PNames,Times,sheetname,ftype=0):
    #    trint(sheetname)
    #    trint(shape(X))
        s = wb.add_worksheet(sheetname)
        s.write(0,0,'ProtNames\time')

        for tj in range(len(Times)):
            s.write(0,tj+1,Times[tj])

        for pj in range(len(PNames)):
            s.write(pj+1,0,PNames[pj])
            for tj in range(len(Times)):
                s.write(pj+1,tj+1,X[tj][pj])
        if(ftype==1):

          s.conditional_format(1,1,len(X[0])-1,len(X), {'type':     'cell',
                                        'criteria': '==',
                                        'value':     1,
                                        'format':    green_format})
        elif(ftype==2):
          s.conditional_format(1,1,len(X[0])-1,len(X), {'type': '3_color_scale',
                                         'min_color': "#00FF00",
                                         'mid_color': "#FBFF80",
                                         'max_color': "#FFFFFF",
                                         'min_type' : 'num',
                                         'mid_type' : 'num',
                                         'max_type' : 'num',
                                         'min_value': '0.045',
                                         'mid_value': '0.052',
                                         'max_value': '0.25',
                                          })

    trint('saving..')
    save(meanData,uProtNames,ExpTimes[0],"mean")
    save([list(a) + [min(a)] for a in Ps],uProtNames+['min p'],rdsS,"p",ftype=2)
    save([list(a) + [min(a)] for a in Psc],uProtNames+['min p'],rdsS,"pc",ftype=2)
    save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.05)],uProtNames+['_Sum'],rdsS,"p_0.05",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.01)],uProtNames+['_Sum'],rdsS,"p_0.01",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.05)],uProtNames+['_Sum'],rdsS,"pc_0.05",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.01)],uProtNames+['_Sum'],rdsS,"pc_0.01",ftype=1)
    wb.close()
    trint('done.')

  """ """
  #