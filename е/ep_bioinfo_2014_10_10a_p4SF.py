import xlrd

fname= 'SpaceFlight_1_10'
fname= 'backup_men_1_6'

rb = xlrd.open_workbook('SFwork/'+fname+'.xls')



ExpData = []
ExpProtNames = []
ExpSubjNums = []
ExpTimes = []

for j in range(len(rb.sheet_names())):
    if(str(j+1) in rb.sheet_names()):
        print('subj %i'%(j+1))
        ExpSubjNums.append(j+1)
        sheet = rb.sheet_by_name(str(j+1))

        ExpTimes.append(sheet.row_values(0,1))
        ExpProtNames.append(sheet.col_values(0,1))

        tData=[]
        for i in range(len(ExpTimes[-1])):
            tData.append(sheet.col_values(i+1,1))
        ExpData.append(tData)

uProtNames = []
for A in ExpProtNames:
    for s in A:
        if(s not in uProtNames):
            uProtNames.append(s)

uProtNames.sort()

uPND = {}
for j in range(len(uProtNames)):
    uPND[uProtNames[j]]=j

uNP = len(uProtNames)


uData = []
for sj in range(len(ExpSubjNums)):
    sData = []    
    for tj in range(len(ExpTimes[sj])):
        sData.append([0]*uNP)
        for pj in range(len(ExpProtNames[sj])):
            sData[-1][uPND[ExpProtNames[sj][pj]]] = ExpData[sj][tj][pj]
    uData.append(sData)


resData = []
rescData = []
meanData = []

from ep_bioinfo_2014_10_08a_ps_of_prots import *

for tj in range(len(ExpTimes[0])):
    TData = [A[tj] for A in uData]
    p,pc=getPs(TData,NTotal=273)
    resData.append(p )
    rescData.append(pc )
    
    mD = [0]*len(TData[0])
    
    for j in range(len(mD)):
        for i in range(len(TData)):
            mD[j]=mD[j]+TData[i][j]
        mD[j]=mD[j]/len(TData)
    meanData.append(mD)
    

resData = array(resData)
rescData =array(rescData)
meanData = array(meanData)
import xlwt

wb=xlwt.Workbook()


def save(X,PNames,Times,sheetname):
    s = wb.add_sheet(sheetname)
    s.write(0,0,'ProtNames\time')
    
    for tj in range(len(Times)):
        s.write(0,tj+1,Times[tj])
    
    for pj in range(len(uProtNames)):
        s.write(pj+1,0,uProtNames[pj])
        for tj in range(len(Times)):
            s.write(pj+1,tj+1,X[tj][pj])
 

save(meanData,uProtNames,ExpTimes[0],"mean")
save(resData,uProtNames,ExpTimes[0],"p-values")
save(rescData,uProtNames,ExpTimes[0],"p-values FDRcorr")
save(1*(resData<0.05),uProtNames,ExpTimes[0],"p_lt_0.05")
save(1*(rescData<0.05),uProtNames,ExpTimes[0],"pFDR_lt_0.05")
save(1*(resData<0.01),uProtNames,ExpTimes[0],"p_lt_0.01")
save(1*(rescData<0.01),uProtNames,ExpTimes[0],"pFDR_lt_0.01")
   
wb.save(    "SFwork/ep_StatSign_totalList_"+fname+".xls" )



#