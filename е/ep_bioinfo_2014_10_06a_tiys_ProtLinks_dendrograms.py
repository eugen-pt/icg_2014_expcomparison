# -*- coding: utf-8 -*-
"""
Created on Mon Oct 06 09:48:12 2014

@author: ep_work
"""

from ep_utilities import *
import random

from ep_bioinfo_2014_10_08b_load_ExpComp_Data import *
""" """

# jaccard index
def JI(l1,l2):
  t = len(set(l1).intersection(set(l2)))
  return t*1.0/(len(l1)+len(l2)-t+0.001)

# Intersection count
def IC(l1,l2):
  return len(set(l1).intersection(set(l2)))

# meet/min

def MM(l1,l2):
  t = len(set(l1).intersection(set(l2)))
  return t*1.0/min(len(l1),len(l2))


""" """

import sklearn.metrics
import scipy.cluster.hierarchy as sch


def myLinkage(A,method=JI):
  L = []


dist = []
for j in range(len(GLists)):
  for i in range(j+1,len(GLists))  :
#    dist.append(1-MM(GLists[j],GLists[i]))
    dist.append(1-JI(GLists[j],GLists[i]))

L=sch.linkage(dist,method='average')



sx=1920
sy=1080
tdpi=200
f=figure('',figsize=(sx, sy), dpi=tdpi)
sch.dendrogram(L,labels=GLNames,orientation = 'left',color_threshold=0.6)
plt.tick_params(axis='y',labelsize=4)
f.savefig('aaa.png',dpi=200)
aaa
""" """

#M = array([ [JI(l1,l2) for l2 in GLists] for l1 in GLists])
M = array([ [MM(l1,l2) for l2 in GLists] for l1 in GLists])
plt.figure('MM vis')
plt.hold(True)

#plt.imshow([ [IC(l1,l2) for l2 in GLists] for l1 in GLists])
plt.imshow(M,interpolation='nearest')

plt.xlim([0,len(GLists)])
plt.ylim([0,len(GLists)])


bound_ticks = copy.copy(tickXs)
bound_ticks.append(tn-1)

center_ticks = [(bound_ticks[j]+bound_ticks[j+1])/2 for j in range(0,len(bound_ticks)-1)]
center_names = tickExps

scenter_names = [s[0:2]+s[-1] for s in tickExps]

#bound_ticks = [x+1 for x in bound_ticks]
bound_ticks[-1]=len(GLists)

for j in range(len(bound_ticks)):
  plot([0,len(GLists)],[bound_ticks[j],bound_ticks[j]],'-k')
  plot([bound_ticks[j],bound_ticks[j]],[0,len(GLists)],'-k')

plt.xticks(bound_ticks+center_ticks,['']*len(bound_ticks)+scenter_names)
plt.yticks(bound_ticks+center_ticks,['']*len(bound_ticks)+center_names)

plt.title('MM')

plt.colorbar()

"""  BuM """


tix = find([s[0]=='B' for s in GLNames])
GLNames=array(GLNames)
GLists = array(GLists)
figure("BuM")

tM = array([ [MM(l1,l2) for l2 in GLists[tix]] for l1 in GLists[tix]])
plt.imshow(tM,interpolation='nearest')

k=0.5
plt.xlim([0-k,len(tix)-k])
plt.ylim([0-k,len(tix)-k])


plt.xticks(range(len(tix)),GLNames[tix], rotation=-90)
plt.yticks(range(len(tix)),GLNames[tix], rotation=0)

plt.title('MM')

plt.colorbar()


""" """

bins = linspace(0,1,num=11)

plt.figure('JI hists vis for vis')
plt.imshow(array([hist(M[j],bins)[0] for j in range(len(M))]).transpose(),interpolation='nearest')

""" """
#

