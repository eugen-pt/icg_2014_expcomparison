# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 12:54:36 2014

@author: ep_work
"""


""" all possible boolean arrays? """

import copy 
import math

def getPs(X,NTotal=-1,):
#  X=array(X)
  if(NTotal<=0):
    NTotal = len(X[0])
  Ps = [sum(A)*1.0/NTotal for A in X] #array(sum(X,axis=1)/NTotal)

#  lP1s = log(Ps)
#  lP0s = log(1-Ps)
  
  P1s = copy.copy(Ps)
  P0s = [1-x for x in Ps]
  lP1s = [math.log(x) for x in P1s]
  lP0s = [math.log(x) for x in P0s]

  N = len(Ps);

#if(1):
#  N=4
  Ns = range(2**N)
  N2s = [2**i for i in range(N+1)]
  A = [];

  for i in range(2**N):
    A.append([(Ns[i]%N2s[k+1])/N2s[k] for k in range(len(N2s)-1)])

#  A = array(A)

  sA = [sum(a) for a in A]

  lPA=[]
  for a in A:
    tlP = 0;
    for pj in range(N):
      if(a[pj]):
        tlP=tlP+lP1s[pj]
      else:
        tlP=tlP+lP0s[pj]
    lPA.append(tlP)    
              
#  lPA = exp(sum(A*lP1s + (1-A)*lP0s,axis=1))
              
  PA = [math.exp(x) for x in lPA]              
              
  rPs = []
  for j in range(N+1):
    t=0
    for i in range(len(sA)):
        if(sA[i]>=j):
            t=t+PA[i]
    rPs.append(t)        
#    rPs.append(sum(lPA[sA>=j]))


  sX = [0]*len(X[0])
  for j in range(len(X)):
      for i in range(len(X[0])):
          sX[i]=sX[i]+X[j][i]
#  sum(X,axis=0)

  xps = [rPs[int(j)] for j in sX]
  return xps
#  xpsc = pFDRadj(xps)

#  return (xps,xpsc)

def ep_b_ec_GoodProts(ProtNames,X,NTotal=-1,pthresh=0.05,corr=1):
  p,pc = getPs(X,NTotal=NTotal)

  if(corr):
    p=pc

  return list(array(ProtNames)[p<pthresh])


#X =  array(uExpData[5])[:,1,:]

#getPs(X)
#plot(sA,log(lPA),'.')
""" """
#

