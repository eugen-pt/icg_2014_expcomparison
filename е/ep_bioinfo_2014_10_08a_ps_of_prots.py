# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 12:54:36 2014

@author: ep_work
"""


""" all possible boolean arrays? """

from numpy import array,shape,sum,NaN
import numpy as np

def pFDRadj(op):
    import numpy as np
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = np.shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();

    realLength=next((i for i, x in enumerate(np.isnan(p[six])) if x), -1 ) #find First
    if(realLength<0):
        realLength = len(p)

    nv = (p[six]*realLength*1.0/(1.0+array(range(realLength)+[NaN]*(len(p)-realLength)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,-1,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    nv[nv>1]=1;
    return nv.reshape(s) #reshaping into original array and returning

def getPs(X,NTotal=-1,):
  X=array(X)
  if(NTotal<=0):
    NTotal = shape(X)[1]
  Ps = array(sum(X,axis=1)/NTotal)
  #Ps = array(sum(X,axis=1)/sum(sum(X,axis=0)>0))

  #Ps = array([0.1]*shape(X)[0])
  #Ps = array([0.1]*18)

  lP1s = np.log(Ps)
  lP0s = np.log(1-Ps)

  N = len(Ps);

  Ns = range(2**N)
  N2s = [2**i for i in range(N+1)]
  A = [];

  for i in range(2**N):
    A.append([(Ns[i]%N2s[k+1])/N2s[k] for k in range(len(N2s)-1)])

  A = array(A)

  sA = sum(A,axis=1)

  lPA = np.exp(sum(A*lP1s + (1-A)*lP0s,axis=1))

  rPs = []
  for j in range(N+1):
    rPs.append(sum(lPA[sA>=j]))


  sX = sum(X,axis=0)

  xps = array([rPs[int(j)] for j in sX])

  xpsc = pFDRadj(xps)

  return (xps,xpsc)

def ep_b_ec_GoodProts(ProtNames,X,NTotal=-1,pthresh=0.05,corr=1):
  p,pc = getPs(X,NTotal=NTotal)

  if(corr):
    p=pc

  return list(array(ProtNames)[p<pthresh])


#X =  array(uExpData[5])[:,1,:]

#getPs(X)
#plot(sA,log(lPA),'.')
""" """
#

