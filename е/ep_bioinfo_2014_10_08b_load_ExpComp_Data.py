# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 12:56:04 2014

@author: ep_work
"""

import xlrd
from ep_utilities import *

# getting all files with experiments

exppath = fullfile(os.getcwd(),'ExpComparison_data_Standard')

files = ep_allfilesin( exppath )

ExpNames = []
ExpData = []
ExpProtNames = []
ExpSubjNums = []


for fs in files:
  if(fs[0]=='!'):
    continue
  ExpNames.append(fs[0:fs.index('.')])

  hData = []
  hProtLists = []

  print(fullfile(exppath,fs))
  rb = xlrd.open_workbook(fullfile(exppath,fs))

  hSubjNums= []
  for j in range(1,40):
    if(str(j) in rb.sheet_names()):
      print('subj %i'%j)
      sheet = rb.sheet_by_name(str(j))
      hSubjNums.append(j)
      hsData = []

      hsProtList = sheet.col_values(0,1)

      tTimesList = sheet.row_values(0)

      # if there's a minus there - take last of 'em
      #  if there isn't any - take the first of all timepoints
      try:
        m_ix = find(array([s[0]=='m' for s in tTimesList]))[-1]
      except:
        m_ix = 1;

      # similar for plus timepoints except we take ether first of 'em or last of all
      try:
        p_ix = find(array([s[0]=='p' for s in tTimesList]))[0]
      except:
        p_ix = len(tTimesList)-1

      print('m = %i (%s)\tp = %i (%s)'%(m_ix,tTimesList[m_ix],p_ix,tTimesList[p_ix]))

      hsData.append(sheet.col_values(m_ix,1))
      hsData.append(sheet.col_values(p_ix,1))

      hProtLists.append(hsProtList)
      hData.append(hsData)

  ExpSubjNums.append(hSubjNums)
  ExpData.append(hData)
  ExpProtNames.append(hProtLists)


  # Getting lists
  ExpProtLists = []
  for expj in range(len(ExpProtNames)):
    eList = []
    for subjj in range(len(ExpProtNames[expj])):
      sList = []
      for timej in range(len(ExpData[expj][subjj])):
        sList.append([ExpProtNames[expj][subjj][k] for k in range(len(ExpProtNames[expj][subjj])) if ExpData[expj][subjj][timej][k] ])

      eList.append(sList)
    ExpProtLists.append(eList)


  # Uniting all the protlists..

  uProtNames = []
  for j in range(len(ExpProtNames)):
    for i in range(len(ExpProtNames[j])):
      uProtNames.extend(ExpProtNames[j][i])
  uProtNames = list(unique(uProtNames))

  uNP = len(uProtNames)
  # forming united array

  uExpData = []
  for expj in range(len(ExpNames)):
    eData = []
    for subjj in range(len(ExpData[expj])):
      sData =np.zeros([len(ExpData[expj][subjj]),uNP])
      for protj in range(len(ExpProtNames[expj][subjj])):
        uix = list(uProtNames).index(ExpProtNames[expj][subjj][protj])
#        print('e%s_s%i_p%i'%(ExpNames[expj],subjj,protj))
        for timej in range(len(ExpData[expj][subjj])):
          try:
            sData[timej,uix] = ExpData[expj][subjj][timej][protj]
          except:
            print([expj,subjj,timej,protj])

            print(ExpData[expj][subjj][timej][protj])
            print(ExpProtNames[expj][subjj][protj])

            raise ValueError()

      eData.append(sData)
    uExpData.append(eData)

  uExpData = array(uExpData)

  for expj in range(len(ExpNames)):
    print('--------------------------------')
    for subjj in range(len(ExpData[expj])):

      for timej in range(len(ExpData[expj][subjj])):
        print('%10s.s%02i..t%i\tpn%i\tpL%i'%(ExpNames[expj],ExpSubjNums[expj][subjj],timej,len(ExpProtNames[expj][subjj]),len(ExpProtLists[expj][subjj][timej])))
      print('---')

""" short expnames """

repDict = {'SpaceFlight':'SF','MARS_500':'M500','MARS_105':'M105','immersion':'Imm','backup_men':'BuM'}

sExpNames = []
for s in ExpNames:
  ts=s
  for k in repDict:
    ts=ts.replace(k,repDict[k])
  sExpNames.append(ts)

""" global lists and data - for all exps all subjects all timepoints """

GLNames = []
GLists  = []
GuData  = []

tickXs = []
tickExps = []

tn=0

if(0): # Experiments
  for expj in range(len(ExpNames)):
    tickXs.append(tn)
    tickExps.append(sExpNames[expj])
    for subjj in range(len(ExpData[expj])):
      for timej in range(len(ExpData[expj][subjj])):
        GLNames.append('%s_s%i_t%i'%(sExpNames[expj],ExpSubjNums[expj][subjj],timej))
        GLists.append(ExpProtLists[expj][subjj][timej])
        GuData.append(uExpData[expj][subjj][timej])
        tn=tn+1
else: #experiments and time !      # i.e. separate groups for different times of experiments (subjects still groupped)
  for expj in range(len(ExpNames)):
#    for timej in [1]:#range(len(ExpData[expj][0])):
    for timej in range(len(ExpData[expj][0])):
      tickXs.append(tn)
      tickExps.append('%s_%i'%(sExpNames[expj],timej))
      for subjj in range(len(ExpData[expj])):
        GLNames.append('%s_s%i_t%i'%(sExpNames[expj],ExpSubjNums[expj][subjj],timej))
        GLists.append(ExpProtLists[expj][subjj][timej])
        GuData.append(uExpData[expj][subjj][timej])
        tn=tn+1

GuData=array(GuData)




""" """
#

