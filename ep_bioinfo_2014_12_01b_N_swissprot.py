# -*- coding: utf-8 -*-
"""
Created on Mon Dec 01 14:27:00 2014

@author: ep_work
"""

print('Getting N_swissprot')

path = '..//..//ICG_bioinfo_stuff_temp_src//HUMAN_9606_idmapping_selected.tab'
path = '..//..//ICG_bioinfo_stuff_temp_src//HUMAN_9606_uniprot-all-reviewed.tab'
SwissProt_IDs = {}

try:
  SwissProt_NP
except:  
  try:
    with open(path) as f:
      for line in f:
        tix = line.index('\t')
        id0= line[0:tix]
        line=line[tix+1:]
        tix = line.index('\t')
        id1 = line[0:tix]
        SwissProt_IDs[id1]=id0
    #    IDs[id0]=id1
    
    SwissProt_NP = len(SwissProt_IDs)
  
  except:
    import urllib2
    R = urllib2.urlopen('http://www.uniprot.org/uniprot/?query=*&fil=organism%3A%22Homo+sapiens+%28Human%29+[9606]%22')
    S = R.read()
  #if(1):
    t0=S.index('reviewed-filter')
    t1=S.index('(',t0)
    t2=S.index(')',t1)
    ts=S[t1+1:t2]
    SwissProt_NP = int(ts.replace(',',''))

print('Got N_swissprot')

""" """
#

