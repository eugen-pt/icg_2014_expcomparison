# -*- coding: utf-8 -*-
"""
Created on Mon Feb  2 13:32:57 2015


requires ep_bioinfo_load105 to have been launched

@author: ep
"""

from ep_bioinfo_2015_01_29a_ps_of_prots_dyns_n_corrs import *

import os
print(os.getcwd())
aaa

hPD = 1.0*s_PD
hParD = 1.0*ParD

hPD = np.swapaxes(np.swapaxes(hPD,0,1),0,2)
hParD = np.swapaxes(np.swapaxes(hParD,0,1),0,2)

ohParD = 1.0*hParD

#hParD[isnan(ohParD)]=0
hPD[isnan(hPD)]=0

# n = normalized

# z=zero norm) m = mean  std = std

znhParD = hParD*1.0
for sj in range(shape(hParD)[1]):
  for parj in range(shape(hParD)[2]):
    znhParD[:,sj,parj] = hParD[:,sj,parj]- mean(hParD[~isnan(hParD[:,sj,parj]),sj,parj])


mnhParD = hParD*1.0
for sj in range(shape(hParD)[1]):
  for parj in range(shape(hParD)[2]):
    mnhParD[:,sj,parj] = hParD[:,sj,parj]/mean(hParD[~isnan(hParD[:,sj,parj]),sj,parj]) - 1

stdnhParD = hParD*1.0
for sj in range(shape(hParD)[1]):
  for parj in range(shape(hParD)[2]):
    stdnhParD[:,sj,parj] = hParD[:,sj,parj]/std(hParD[~isnan(hParD[:,sj,parj]),sj,parj])
    stdnhParD[:,sj,parj] = stdnhParD[:,sj,parj] - mean(stdnhParD[~isnan(stdnhParD[:,sj,parj]),sj,parj])

znhParD[isnan(ohParD)]=0
mnhParD[isnan(ohParD)]=0
stdnhParD[isnan(ohParD)]=0

znhParD_der = 1.0*znhParD
mnhParD_der = 1.0*mnhParD
stdnhParD_der = 1.0*stdnhParD

for sj in range(shape(znhParD)[1]):
  for pj in range(shape(znhParD)[2]):
    znhParD_der[:,sj,pj] = np.gradient(znhParD[:,sj,pj])
    mnhParD_der[:,sj,pj] = np.gradient(mnhParD[:,sj,pj])
    stdnhParD_der[:,sj,pj] = np.gradient(stdnhParD[:,sj,pj])


# c = combined (times*people, parameters)

cznhParD=mnhParD.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')
cmnhParD=mnhParD.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')
cstdnhParD=stdnhParD.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')

cznhParD_der=mnhParD_der.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')
cmnhParD_der=mnhParD_der.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')
cstdnhParD_der=stdnhParD_der.reshape((shape(mnhParD)[0]*shape(mnhParD)[1],shape(mnhParD)[2]),order='F')


""" """
#

