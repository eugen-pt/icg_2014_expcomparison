import xlrd


fname= 'backup_men_1_6'
ds = [[1,0],[0,1],[1,1],[-1,1]]
fname= 'SpaceFlight_1_10'
ds = [[1,0,0],[0,1,0],[0,0,1],[1,1,1],[-1,1,0],[-1,0,1],[0,-1,1],[-1,2,-1],[-2,1,1],[1,1,-2]]

rb = xlrd.open_workbook('SFwork/'+fname+'.xls')

nMax=21


ExpData = []
ExpProtNames = []
ExpSubjNums = []
ExpTimes = []

for j in range(len(rb.sheet_names())):
    if(str(j+1) in rb.sheet_names()):
        print('subj %i'%(j+1))
        ExpSubjNums.append(j+1)
        sheet = rb.sheet_by_name(str(j+1))

        ExpTimes.append(sheet.row_values(0,1))
        ExpProtNames.append(sheet.col_values(0,1))

        tData=[]
        for i in range(len(ExpTimes[-1])):
            tData.append(sheet.col_values(i+1,1))
        ExpData.append(tData)

try:
  upn
  uProtNames = upn
  usedAll=1
except:
  uProtNames = []
  for A in ExpProtNames:
      for s in A:
          if(s not in uProtNames):
              uProtNames.append(s)

  uProtNames.sort()
  usedAll=0

uPND = {}
for j in range(len(uProtNames)):
    uPND[uProtNames[j]]=j

uNP = len(uProtNames)


uData = []
for sj in range(len(ExpSubjNums)):
    sData = []
    for tj in range(len(ExpTimes[sj])):
        sData.append([0]*uNP)
        for pj in range(len(ExpProtNames[sj])):
            sData[-1][uPND[ExpProtNames[sj][pj]]] = ExpData[sj][tj][pj]
    uData.append(sData)


X = np.swapaxes(uData,0,1)

#aaa

resData = []
rescData = []
meanData = mean(uData,axis=0)

from ep_bioinfo_2014_11_13a_ps_of_prots_dyns import *


rds = []
Ps = []
Psc = []
for d in ds:
  t = getDynPs(X,d,nMax=nMax)
  rds.append(d)
  Ps.append(t[0][0])
  Psc.append(t[1][0])
  td = list(-1.0*array(d))
  t = getDynPs(X,td,nMax=nMax)
  rds.append(td)
  Ps.append(t[0][0])
  Psc.append(t[1][0])



import xlwt

wb=xlwt.Workbook()


def save(X,PNames,Times,sheetname):
#    print(sheetname)
#    print(shape(X))
    s = wb.add_sheet(sheetname)
    s.write(0,0,'ProtNames\time')

    for tj in range(len(Times)):
        s.write(0,tj+1,Times[tj])

    for pj in range(len(PNames)):
        s.write(pj+1,0,PNames[pj])
        for tj in range(len(Times)):
            s.write(pj+1,tj+1,X[tj][pj])
print('saving..')
save(meanData,uProtNames,ExpTimes[0],"mean")
save([list(a) + [min(a)] for a in Ps],uProtNames+['min p'],[ '['+','.join(map(str,a))+']' for a in rds],"p")
save([list(a) + [min(a)] for a in Psc],uProtNames+['min p'],[ '['+','.join(map(str,a))+']' for a in rds],"pc")
save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.05)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"p_0.05")
save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.01)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"p_0.01")
save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.05)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"pc_0.05")
save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.01)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"pc_0.01")
wb.save(    "SFwork/ep_StatSign_Dynv0_"+str(usedAll)+"_"+fname+".xls" )

print('done.')

""" """
#