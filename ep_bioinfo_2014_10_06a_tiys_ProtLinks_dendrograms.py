# -*- coding: utf-8 -*-
"""
Created on Mon Oct 06 09:48:12 2014

@author: ep_work
"""

from ep_utilities import *
import random
NL = 8;

if(0):
  Lists = []
  random.seed(0)
  for j in range(NL):
    Lists.append(random.sample(range(100),random.randint(20,50)) )
else:
  import xlrd

  # getting all files with experiments

  exppath = fullfile(os.getcwd(),'ExpComparison_data_Standard')

  files = ep_allfilesin( exppath )

  ExpNames = []
  ExpData = []
  ExpProtNames = []
  ExpSubjNums = []


  for fs in files:
    if(fs[0]=='!'):
      continue
    ExpNames.append(fs[0:fs.index('.')])

    hData = []
    hProtLists = []

    print(fullfile(exppath,fs))
    rb = xlrd.open_workbook(fullfile(exppath,fs))

    hSubjNums= []
    for j in range(1,40):
      if(str(j) in rb.sheet_names()):
        print('subj %i'%j)
        sheet = rb.sheet_by_name(str(j))
        hSubjNums.append(j)
        hsData = []

        hsProtList = sheet.col_values(0,1)

        tTimesList = sheet.row_values(0)

        # if there's a minus there - take last of 'em
        #  if there isn't any - take the first of all timepoints
        try:
          m_ix = find(array([s[0]=='m' for s in tTimesList]))[-1]
        except:
          m_ix = 1;

        # similar for plus timepoints except we take ether first of 'em or last of all
        try:
          p_ix = find(array([s[0]=='p' for s in tTimesList]))[0]
        except:
          p_ix = len(tTimesList)-1

        print('m = %i (%s)\tp = %i (%s)'%(m_ix,tTimesList[m_ix],p_ix,tTimesList[p_ix]))        
        
        hsData.append(sheet.col_values(m_ix,1))
        hsData.append(sheet.col_values(p_ix,1))

        hProtLists.append(hsProtList)
        hData.append(hsData)

    ExpSubjNums.append(hSubjNums)
    ExpData.append(hData)
    ExpProtNames.append(hProtLists)


  # Getting lists
  ExpProtLists = []
  for expj in range(len(ExpProtNames)):
    eList = []
    for subjj in range(len(ExpProtNames[expj])):
      sList = []
      for timej in range(len(ExpData[expj][subjj])):
        sList.append([ExpProtNames[expj][subjj][k] for k in range(len(ExpProtNames[expj][subjj])) if ExpData[expj][subjj][timej][k] ])

      eList.append(sList)
    ExpProtLists.append(eList)


  # Uniting all the protlists..

  uProtNames = []
  for j in range(len(ExpProtNames)):
    for i in range(len(ExpProtNames[j])):
      uProtNames.extend(ExpProtNames[j][i])
  uProtNames = list(unique(uProtNames))
  
  uNP = len(uProtNames)
  # forming united array
  
  uExpData = []
  for expj in range(len(ExpNames)):
    eData = []
    for subjj in range(len(ExpData[expj])):
      sData =np.zeros([len(ExpData[expj][subjj]),uNP])
      for protj in range(len(ExpProtNames[expj][subjj])):
        uix = list(uProtNames).index(ExpProtNames[expj][subjj][protj])
#        print('e%s_s%i_p%i'%(ExpNames[expj],subjj,protj))
        for timej in range(len(ExpData[expj][subjj])):
          try:
            sData[timej,uix] = ExpData[expj][subjj][timej][protj]
          except:
            print([expj,subjj,timej,protj])
            
            print(ExpData[expj][subjj][timej][protj])
            print(ExpProtNames[expj][subjj][protj])
            
            raise ValueError()
            
      eData.append(sData)  
    uExpData.append(eData)

  uExpData = array(uExpData)  
  
  for expj in range(len(ExpNames)):
    print('--------------------------------')
    for subjj in range(len(ExpData[expj])):
      
      for timej in range(len(ExpData[expj][subjj])):
        print('%10s.s%02i..t%i\tpn%i\tpL%i'%(ExpNames[expj],ExpSubjNums[expj][subjj],timej,len(ExpProtNames[expj][subjj]),len(ExpProtLists[expj][subjj][timej])))
      print('---')

""" short expnames """

repDict = {'SpaceFlight':'SF','MARS_500':'M500','MARS_105':'M105','immersion':'Imm','backup_men':'BuM'}

sExpNames = []
for s in ExpNames:
  ts=s
  for k in repDict:
    ts=ts.replace(k,repDict[k])
  sExpNames.append(ts)
  
""" global lists and data - for all exps all subjects all timepoints """

GLNames = []
GLists  = []
GuData  = []

tickXs = []
tickExps = []

tn=0

if(0): # Experiments
  for expj in range(len(ExpNames)):
    tickXs.append(tn)
    tickExps.append(sExpNames[expj])
    for timej in range(len(ExpData[expj][subjj])):
      for subjj in range(len(ExpData[expj])):
        GLNames.append('%s_s%i_t%i'%(sExpNames[expj],ExpSubjNums[expj][subjj],timej))
        GLists.append(ExpProtLists[expj][subjj][timej])
        GuData.append(uExpData[expj][subjj][timej])
        tn=tn+1
else: #experiments and time !      # i.e. separate groups for different times of experiments (subjects still groupped)
  for expj in range(len(ExpNames)):
    for timej in range(len(ExpData[expj][0])):
      tickXs.append(tn)
      tickExps.append('%s_%i'%(sExpNames[expj],timej))
      for subjj in range(len(ExpData[expj])):
        GLNames.append('%s_s%i_t%i'%(sExpNames[expj],ExpSubjNums[expj][subjj],timej))
        GLists.append(ExpProtLists[expj][subjj][timej])
        GuData.append(uExpData[expj][subjj][timej])
        tn=tn+1
  
GuData=array(GuData)


      
""" """

# jaccard index
def JI(l1,l2):
  t = len(set(l1).intersection(set(l2)))
  return t/(len(l1)+len(l2)-t+0.001)

# Intersection count
def IC(l1,l2):
  return len(set(l1).intersection(set(l2)))

""" """

import sklearn.metrics
import scipy.cluster.hierarchy as sch


def myLinkage(A,method=JI):
  L = []
  
  
dist = []
for j in range(len(GLists)):
  for i in range(j+1,len(GLists))  :
    dist.append(1-JI(GLists[j],GLists[i]))

L=sch.linkage(GuData,method='average',metric = 'cityblock')
L=sch.linkage(dist,method='complete')
L=sch.linkage(dist,method='single')
L=sch.linkage(dist,method='average')



sx=1920
sy=1080
tdpi=400
f=figure('',figsize=(sx, sy), dpi=tdpi)
sch.dendrogram(L,labels=GLNames,orientation = 'left')
plt.tick_params(axis='y',labelsize=4)
f.savefig('aaa.png',dpi=tdpi)
aaa
""" """

plt.figure('JI vis')
plt.hold(True)

M = array([ [JI(l1,l2) for l2 in GLists] for l1 in GLists])
#plt.imshow([ [IC(l1,l2) for l2 in GLists] for l1 in GLists])
plt.imshow([ [JI(l1,l2) for l2 in GLists] for l1 in GLists],interpolation='nearest')

plt.xlim([-1,len(GLists)])
plt.ylim([-1,len(GLists)])


bound_ticks = copy.copy(tickXs)
bound_ticks.append(tn-1)

center_ticks = [(bound_ticks[j]+bound_ticks[j+1])/2 for j in range(0,len(bound_ticks)-1)]
center_names = tickExps

#bound_ticks = [x+1 for x in bound_ticks]
bound_ticks[-1]=len(GLists)

for j in range(len(bound_ticks)):
  plot([0,len(GLists)],[bound_ticks[j],bound_ticks[j]],'-k')
  plot([bound_ticks[j],bound_ticks[j]],[0,len(GLists)],'-k')

plt.xticks(bound_ticks+center_ticks,['']*len(bound_ticks)+center_names)
plt.yticks(bound_ticks+center_ticks,['']*len(bound_ticks)+center_names)

plt.title('JI')

plt.colorbar()

""" trying to distinguish those outliers.. """

bins = linspace(0,1,num=101)

plt.figure('JI hists vis for vis')
plt.imshow(array([hist(M[j],bins)[0] for j in range(len(M))]).transpose(),interpolation='nearest')

""" """
#

