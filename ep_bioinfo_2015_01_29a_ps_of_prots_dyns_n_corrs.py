# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 12:54:36 2014

ep_bioinfo_2015_01_29a_ps_of_prots_dyns_n_corrs

@author: ep_work
"""


""" all possible boolean arrays? """

from numpy import array,shape,sum,NaN
import numpy as np
from numpy import *

def pFDRadj(op):
    import numpy as np
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = np.shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();

    realLength=next((i for i, x in enumerate(np.isnan(p[six])) if x), -1 ) #find First
    if(realLength<0):
        realLength = len(p)

    nv = (p[six]*realLength*1.0/(1.0+array(range(realLength)+[NaN]*(len(p)-realLength)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,-1,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    nv[nv>1]=1;
    return nv.reshape(s) #reshaping into original array and returning

def arrOfBinary(N):
  N2s = [2**i for i in range(N+1)]
  A = [];

  i=0l
  while i<2**N:
    A.append([int((i%N2s[k+1])/N2s[k]) for k in range(N)])
    i=i+1
  return A

def arrOfBinary2(N,NMax = 15):
  N2s = [2**i for i in range(N+1)]
  A = [];

  i=0l
  while i<2**NMax:
    t=long(np.random.rand(1)*(2l**N))
    A.append([int((t%N2s[k+1])/N2s[k]) for k in range(N)])
    i=i+1
  return A


ep_pProts_N=0
ep_pProts_A=[]

#
#  X is an array (people x proteins)
#
#   p-values are returned per protein
#     -as a P of it having been identified in this many people or more
#               (for completely random reasons)
def getPs(X,NTotal=-1,mode=0,nMax=-1,nS=10):
#  print('s')
  X=array(X)
  if(NTotal<=0):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal)
  #Ps = array(sum(X,axis=1)/sum(sum(X,axis=0)>0))

  #Ps = array([0.1]*shape(X)[0])
  #Ps = array([0.1]*18)


  N = len(Ps);

  if(nMax==-1):
    nMax=N


  global  ep_pProts_N
  global  ep_pProts_A

  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    while(j<2l**nMax):
      tis=sum(np.random.rand(2**nS,N)<Ps,axis=1)
      for ti in tis:
        rPs[ti]=rPs[ti]+1.0
      j=j+2**nS

    rPs=rPs/sum(rPs)
    #lPA as result - counts per sum
  else:
    lP1s = np.log(Ps)
    lP0s = np.log(1-Ps)
    if(ep_pProts_N==N):
      A = ep_pProts_A
    else:
#      print('a')
      if(mode>0):
        if(mode==1):
          A=array(arrOfBinary(nMax))
        else:
          A=array(arrOfBinary2(nMax))
      else:
        if(N<=18):
          A=array(arrOfBinary(nMax))
        else:
          A=array(arrOfBinary2(nMax,NMax=18))

      ep_pProts_N = N
      ep_pProts_A = A
#      print('ae')


    sA = sum(A,axis=1)

    lPA = np.exp(sum(A*lP1s + (1-A)*lP0s,axis=1))

    lPA=lPA/sum(lPA)

    rPs = []
    for j in range(N+1):
      rPs.append(sum(lPA[sA==j]))


  sX = sum(X,axis=0)


  xps = array([sum(rPs[int(j):]) for j in sX])

  xpsc = pFDRadj(xps)

  return (xps,xpsc)


def timeTestX(NRT=10,NS=10,NP=100,NTotal=-1,mode=0,nMax=-1,nS=10):
  from time import clock
  X = 1.0*(np.random.rand(NS,NP)<0.3)
  tStart=clock()
  print('%f %s'%(clock()-tStart,'starting..'))
  x=[]
  for j in range(NRT):
    x.append(getPs(X,NTotal=NTotal,mode=mode,nMax=nMax,nS=nS))
  print('%f %s'%(clock()-tStart,'done'))



def ep_b_ec_GoodProts(ProtNames,X,NTotal=-1,pthresh=0.05,corr=1):
  p,pc = getPs(X,NTotal=NTotal)

  if(corr):
    p=pc

  return list(array(ProtNames)[p<pthresh])

""" Dynamics """

# simple 1-dimensional dynamic is considered
def getDynPs1d(X,d,mode=3,nMax = 15,nS=10,NTotal=-153,debug=0):
#  X = array([[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,1]])

#  d = [0,1,-1]
#  d = [1,1,1]

  X=array(X)
  if(NTotal==-153):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal)

  if(debug):
    print('Ps:')
    print(Ps)

  if(debug):
    print('dyn:')
    print(d)

  N = len(Ps);

#  nMax=N
#  nMax=10

#  nS=10

  dX = sum(X.transpose()*d,axis=1)
  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    dvalues = []
    dsums = {}

    while(j<2l**nMax):
      tis=sum((np.random.rand(2**nS,N)<Ps)*d,axis=1)
      for ti in tis:
        try:
          dsums[ti]=dsums[ti]+1.0
        except:
          dsums[ti]=1.0
  #      rPs[ti]=rPs[ti]+1.0
      j=j+2l**nS

    for dx in dX:
      if(dx not in dsums):
        dsums[dx]=0
    rPvalues = array(dsums.keys())
    rPs =  array([dsums[s] for s in rPvalues])
    rPs=rPs/sum(rPs)

    tix = np.argsort(rPvalues)
    rPs=rPs[tix]
    rPvalues = rPvalues[tix]
    rPvD = {rPvalues[j]:j for j in range(len(rPvalues))}

    if(debug):
      print('rPvalues:')
      print(rPvalues)
      print('rPs')
      print(rPs)

  xps = (array([sum(rPs[rPvD[dx]:]) for dx in dX]),array([sum(rPs[:rPvD[dx]+1]) for dx in dX]))
  xpsc = [pFDRadj(a) for a in xps]

  return (xps,xpsc)

# simple 1-dimensional dynamic is considered
def getDynPs1d_m(X= array([[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,1]]),ds=array([[0,1,-1],[1,1,1]]),mode=3,nMax = 15,nS=10,NTotal=-153,debug=0):
#  X = array([[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,1]])

#  d = [0,1,-1]
#  d = [1,1,1]
#if(1):
#  X= array([[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,1]])
#  ds=array([[0,1,-1],[1,1,1]])
##  nMax=N
#  nMax=10
#  mode = 3
#  nS=10

  if(NTotal==-153):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal).transpose()

  if(debug):
    print('Ps:')
    print(Ps)

  N = len(Ps);


  X=matrix(X)


  ds = matrix(ds)
  ds = ds.transpose()

  ND = shape(ds)[1]

  dX = X.transpose()*ds

  nMax2 = 2l**nMax
  nS2 = 2l**nS
  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    dsums = [{} for j in range(ND)]

    while(j<nMax2):
      tis=(np.random.rand(nS2,N)<Ps)*ds
      for dj in range(ND):
        for tij in range(nS2):
          ti = tis[tij,dj]
#          print(ti)
#          print(dsums[dj].keys())
          try:
            dsums[dj][ti]=dsums[dj][ti]+1.0
          except:
            dsums[dj][ti]=1.0
  #      rPs[ti]=rPs[ti]+1.0
      j=j+nS2

    for dj in range(ND):
      for dxj in range(shape(dX)[0]):
        if(dX[dxj,dj] not in dsums[dj]):
          dsums[dj][dX[dxj,dj]]=0

    xps=[]
    xpsc=[]
    for dj in range(ND):
      if(debug):
        print('dyn:')
        print(ds[:,dj].transpose())
      rPvalues = dsums[dj].keys()
#      print(rPvalues)

      rPs =  array([dsums[dj][s] for s in rPvalues])
      rPs=rPs/sum(rPs)

      tix = np.argsort(rPvalues)
      rPs=rPs[tix]
      rPvalues = list(array(rPvalues)[tix])
      rPvD = {rPvalues[j]:j for j in range(len(rPvalues))}


      if(debug):
        print('rPvalues:')
        print(rPvalues)
        print('rPs')
        print(rPs)

      xps.append([array([sum(rPs[rPvD[dX[dxj,dj]]:]) for dxj in range(shape(dX)[0])]),array([sum(rPs[:rPvD[dX[dxj,dj]]+1]) for dxj in range(shape(dX)[0])])])
      xpsc.append([pFDRadj(a) for a in xps[dj]])

  return (xps,xpsc)



# Xs = [times,people,proteins]
# ds = [times] or ds = [dynamics*times]
def getDynPs(Xs,ds,mode=3,nMax = 15,nS=10,NTotal=-153,debug=0):

  Xs=array(Xs)
  ds=array(ds)


  X=Xs.reshape((shape(Xs)[0]*shape(Xs)[1],shape(Xs)[2]),order='F')
  if(len(shape(ds))>1):
    print('Using multiple dyns!')
    ds=[list(d)*shape(Xs)[1] for d in ds]

    return getDynPs1d_m(X,ds,mode=mode,nMax = nMax,nS=nS,NTotal=NTotal,debug=debug)
  else:
    if(shape(Xs)[0]==len(ds)):
      d=list(ds)*shape(Xs)[1]
    else:
      if(len(ds)==shape(X)[0]):
        d=ds
      else:
        raise ValueError('shape(Xs,0)!=len(ds) and shape(X)[0]!=len(ds)')
        

    return getDynPs1d(X,d,mode=mode,nMax = nMax,nS=nS,NTotal=NTotal,debug=debug)


""" """

# Xs = [times,people,proteins]
def getNConstPs(Xs,mode=3,nMax = 15,nS=10,NTotal=-153,debug=0):
  Xs = array(Xs)

  MS = shape(Xs)[1]
  MT = shape(Xs)[0]

  NP = shape(Xs)[2]

  tds = []
  for tj in range(MT):
    temp = [0 for i in range(MT)]
    temp[tj]=1
    tds.append(temp)

  if(debug):
    print('tds:')
    print(tds)

  X=Xs.reshape((shape(Xs)[0]*shape(Xs)[1],shape(Xs)[2]),order='F')
  ds=[list(d)*shape(Xs)[1] for d in tds]

  # from   getDynPs1d_m

  if(NTotal==-153):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal).transpose()

  if(debug):
    print('Ps:')
    print(Ps)

  N = len(Ps);


  X=np.matrix(X)


  ds = np.matrix(ds)
  ds = ds.transpose()

  ND = shape(ds)[1]

  dX = X.transpose()*ds
  mdX = sum(dX,axis=1)*1.0/ND
  dX = np.array(sum(np.multiply(dX,dX),axis=1)*1.0/ND-np.multiply(mdX,mdX)).transpose()[0]

  if(debug):
    print('dX:')
    print(dX)

  nMax2 = 2l**nMax
  nS2 = 2l**nS
  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    dsums = {}

    while(j<nMax2):
      tis=(np.random.rand(nS2,N)<Ps)*ds
      tms = sum(tis,axis=1)*1.0/ND
      tms = sum(np.multiply(tis,tis),axis=1)*1.0/ND-np.multiply(tms,tms)
      for tij in range(nS2):
        ti = tms[tij,0]
#        print(ti)
#        raise ValueError('ss')
#          print(dsums[dj].keys())
        try:
          dsums[ti]=dsums[dj][ti]+1.0
        except:
          dsums[ti]=1.0
  #      rPs[ti]=rPs[ti]+1.0
      j=j+nS2

    for dxj in range(len(dX)):
      if(dX[dxj] not in dsums):
        dsums[dX[dxj]]=0


    rPvalues = dsums.keys()
#      print(rPvalues)

    rPs =  array([dsums[s] for s in rPvalues])
    rPs=rPs/sum(rPs)

    tix = np.argsort(rPvalues)
    rPs=rPs[tix]
    rPvalues = list(array(rPvalues)[tix])

    if(debug):
      print('rPvalues:')
      print(rPvalues)
      print('rPs')
      print(rPs)

    rPvD = {rPvalues[j]:j for j in range(len(rPvalues))}




    xps = [array([sum(rPs[rPvD[dX[dxj]]:]) for dxj in range(shape(dX)[0])]),array([sum(rPs[:rPvD[dX[dxj]]+1]) for dxj in range(shape(dX)[0])])]
    xpsc = [pFDRadj(a) for a in xps]

  return (xps,xpsc)


""" Correlation-based stuff """

# simple 1-dimensional dynamic is considered
def getCorrPs1d(X,d,mode=3,nMax = 15,nS=10,NTotal=-153,debug=0,NaNs_as_zeros=1):
#  X = array([[1,1,0,0,0],[0,1,1,0,0],[0,0,1,1,1]])

#  d = [0,1,-1]
#  d = [1,1,1]

  X=array(X)
  if(NTotal==-153):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal)

  if(debug):
    print('X:')
    print(X)
  if(debug):
    print('Ps:')
    print(Ps)

  if(debug):
    print('corr dyn:')
    print(d)

  N = len(Ps);



#  nMax=N
#  nMax=10

#  nS=10

#  dX = sum(X.transpose()*d,axis=1)
  dX = np.corrcoef(X.transpose(),d)[:-1,-1]
  if(NaNs_as_zeros):
    dX[isnan(dX)]=0
  if(debug):
    print('dX:')
    print(dX)
  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    dvalues = []
    dsums = {}

    while(j<2l**nMax):
      R =  (np.random.rand(2**nS,N)<Ps)  # random stuff 
#      tis=[]
#      for j in range(2**nS):
#          tis.append(np.corrcoef(R[j,:],d)[0,1])
      tis = np.corrcoef(R,d)[:-1,-1]
      for ti_s in tis:
        if(isnan(ti_s)):
          ti=0
        else:
          ti=ti_s
        if((isnan(ti_s)) and (NaNs_as_zeros==0)):
          continue
        try:
          dsums[ti]=dsums[ti]+1.0
        except:
          dsums[ti]=1.0
  #      rPs[ti]=rPs[ti]+1.0
      j=j+2l**nS

    if(debug):
      print('dsums:')
      print(dsums)
      
    safe_dX = []  
    for dx in dX:
      if(isnan(dx)):
        pass
      else:
        safe_dX.append(dx)
        if(dx not in dsums):
          dsums[dx]=0
    rPvalues = array(dsums.keys())
    rPs =  array([dsums[s] for s in rPvalues])
    rPs=rPs/sum(rPs)

    tix = np.argsort(rPvalues)
    rPs=rPs[tix]
    rPvalues = rPvalues[tix]
    rPvD = {rPvalues[j]:j for j in range(len(rPvalues))}

    if(debug):
      print('rPvalues:')
      print(rPvalues)
      print('rPs')
      print(rPs)

  xps = ([sum(rPs[rPvD[dx]:]) for dx in safe_dX],[sum(rPs[:rPvD[dx]+1]) for dx in safe_dX])
  xpsc = [list(pFDRadj(a)) for a in xps]
  
  for j in range(len(xps)):
    for dxj in range(len(dX)):
      if(isnan(dX[dxj])):
        xps[j].insert(dxj,2)
        xpsc[j].insert(dxj,2)
  xps=(array(xps[0]),array(xps[1]))    
  xpsc=(array(xpsc[0]),array(xpsc[1]))    

  if(debug):
    print('P values +:')
    print(' '.join(['%.2f'%a for a in xps[0]]))
    print('P values -:')
    print(' '.join(['%.2f'%a for a in xps[1]]))
  return (xps,xpsc)

# Xs = [times,people,proteins]
def getCorrPs(Xs,ds,mode=3,nMax = 15,nS=10,NTotal=-153,debug=0):

  Xs=array(Xs)
  ds=array(ds)


  X=Xs.reshape((shape(Xs)[0]*shape(Xs)[1],shape(Xs)[2]),order='F')
  if(len(shape(ds))>1):
    print('Using multiple dyns!')
    xps = []
    xpsc = []
    for d in ds:
      td = list(d)*shape(Xs)[1]
      A,B = getCorrPs1d(X,td,mode=mode,nMax = nMax,nS=nS,NTotal=NTotal,debug=debug)
      xps.append(A)
      apsc.append(B)
    return xps,xpsc  
  else:
    if(shape(Xs)[0]!=len(ds)):
      raise ValueError('shape(X,0)!=len(ds)')
    d=list(ds)*shape(Xs)[1]

    return getCorrPs1d(X,d,mode=mode,nMax = nMax,nS=nS,NTotal=NTotal,debug=debug)

""" """
#

