# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 12:54:36 2014

@author: ep_work
"""


""" all possible boolean arrays? """

from numpy import array,shape,sum,NaN
import numpy as np

def pFDRadj(op):
    import numpy as np
    # http://brainder.org/2011/09/05/fdr-corrected-fdr-adjusted-p-values/
    #  Yekutieli and Benjamini (1999)
    #
    #  Simple adjustment of BH thresholding operation to produce adjusted p-values
    #   which can be thresholded.  (p_adj(p) is monotonic)
    s = np.shape(op)
    p=np.array(op).flatten()
    six = np.array(p).argsort();

    realLength=next((i for i, x in enumerate(np.isnan(p[six])) if x), -1 ) #find First
    if(realLength<0):
        realLength = len(p)

    nv = (p[six]*realLength*1.0/(1.0+array(range(realLength)+[NaN]*(len(p)-realLength)))) # 'corrected' values

#    nv = [ min(nv[i:]) for i in range(len(nv))] ## the adjustment itself
    #nnv=[nv[-1]]
    for j in range(len(nv)-2,-1,-1):
       if(nv[j]>nv[j+1]):
           nv[j]=nv[j+1];

    nv = array(nv)[six.argsort()] #returning the original order
    nv[nv>1]=1;
    return nv.reshape(s) #reshaping into original array and returning

def arrOfBinary(N):
  N2s = [2**i for i in range(N+1)]
  A = [];

  i=0l
  while i<2**N:
    A.append([int((i%N2s[k+1])/N2s[k]) for k in range(N)])
    i=i+1
  return A

def arrOfBinary2(N,NMax = 15):
  N2s = [2**i for i in range(N+1)]
  A = [];

  i=0l
  while i<2**NMax:
    t=long(np.random.rand(1)*(2l**N))
    A.append([int((t%N2s[k+1])/N2s[k]) for k in range(N)])
    i=i+1
  return A


ep_pProts_N=0
ep_pProts_A=[]

#
#  X is an array (people x proteins)
#
#   p-values are returned per protein
#     -as a P of it having been identified in this many people or more
#               (for completely random reasons)
def getPs(X,NTotal=-1,mode=0,nMax=-1,nS=10):
#  print('s')
  X=array(X)
  if(NTotal<=0):
    NTotal = shape(X)[1]
  Ps = array(np.sum(X,axis=1)*1.0/NTotal)
  #Ps = array(sum(X,axis=1)/sum(sum(X,axis=0)>0))

  #Ps = array([0.1]*shape(X)[0])
  #Ps = array([0.1]*18)


  N = len(Ps);

  if(nMax==-1):
    nMax=N


  global  ep_pProts_N
  global  ep_pProts_A

  if(mode==3):
    rPs = [0.0]*(N+1)


    j=0l

    while(j<2l**nMax):
      tis=sum(np.random.rand(2**nS,N)<Ps,axis=1)
      for ti in tis:
        rPs[ti]=rPs[ti]+1.0
      j=j+2**nS

    rPs=rPs/sum(rPs)
    #lPA as result - counts per sum
  else:
    lP1s = np.log(Ps)
    lP0s = np.log(1-Ps)
    if(ep_pProts_N==N):
      A = ep_pProts_A
    else:
#      print('a')
      if(mode>0):
        if(mode==1):
          A=array(arrOfBinary(nMax))
        else:
          A=array(arrOfBinary2(nMax))
      else:
        if(N<=18):
          A=array(arrOfBinary(nMax))
        else:
          A=array(arrOfBinary2(nMax,NMax=18))

      ep_pProts_N = N
      ep_pProts_A = A
#      print('ae')


    sA = sum(A,axis=1)

    lPA = np.exp(sum(A*lP1s + (1-A)*lP0s,axis=1))

    lPA=lPA/sum(lPA)

    rPs = []
    for j in range(N+1):
      rPs.append(sum(lPA[sA==j]))


  sX = sum(X,axis=0)


  xps = array([sum(rPs[int(j):]) for j in sX])

  xpsc = pFDRadj(xps)

  return (xps,xpsc)


def timeTestX(NRT=10,NS=10,NP=100,NTotal=-1,mode=0,nMax=-1,nS=10):
  from time import clock
  X = 1.0*(np.random.rand(NS,NP)<0.3)
  tStart=clock()
  print('%f %s'%(clock()-tStart,'starting..'))
  x=[]
  for j in range(NRT):
    x.append(getPs(X,NTotal=NTotal,mode=mode,nMax=nMax,nS=nS))
  print('%f %s'%(clock()-tStart,'done'))



def ep_b_ec_GoodProts(ProtNames,X,NTotal=-1,pthresh=0.05,corr=1):
  p,pc = getPs(X,NTotal=NTotal)

  if(corr):
    p=pc

  return list(array(ProtNames)[p<pthresh])


#X =  array(uExpData[5])[:,1,:]

#getPs(X)
#plot(sA,log(lPA),'.')
""" """
#

