
"""


requires qp_bioinfo_load105 and ep_bioinfo_2015_02_02a_105dyns to have been launched

"""

import xlrd
from time import clock


#from ep_bioinfo_2015_02_02a_105dyns import *
from ep_bioinfo_2014_12_01b_N_swissprot import *

from ep_bioinfo_2015_01_29a_ps_of_prots_dyns_n_corrs import *

from xlsxwriter.workbook import Workbook

tStart = clock()


def trint(s):
  print('%5.2f\t%s'%(clock()-tStart,s))

nMax=26
nMax=24
nMax=20
#nMax=18


fnames = ['noNorm','noNorm_derivative','meanNorm','meanNorm_derivative','stdNorm'  ,'stdNorm_derivative']
dss    = [cznhParD,cznhParD_der       ,cmnhParD  ,cmnhParD_der         , cstdnhParD,cstdnhParD_der      ]

for j in range(len(dss)):
  dss[j]=dss[j].transpose()


NTotalNames = ['AllHere','AllSwissProt']
NTotals     = [-153     ,SwissProt_NP ]

#fnames = [fnames[0]]

for FileJ in range(len(fnames)):
  fname =  fnames[FileJ]
  print(fname)
  ds=dss[FileJ]
#  cor_ds=cor_dss[FileJ]
#  subj_list=subj_lists[FileJ]


  uProtNames = list(s_ProtNames)
  
  X = hPD

  #aaa

  resData = []
  rescData = []
  meanData = mean(X,axis=1)

  for Nj in range(len(NTotals)):
    NTotal= NTotals[Nj]

    rdsS = []
    rds = []
    Ps = []
    Psc = []

    MultiD = 0 # 1 is about 2 times slower =(

    for dj in range(len(ds)):
      trint('f %i/%i N %i/%i dyn %i/%i'%(FileJ,len(fnames),Nj,len(NTotals),dj,len(ds)))
      d=ds[dj]
      t = getDynPs(X,d,nMax=nMax,NTotal=NTotal)
      rds.append(d)
#      rdsS.append('['+','.join(map(str,d))+']')
      rdsS.append('_+_p%i_%s'%(dj+1,ParNames[dj]))
      Ps.append(t[0][0])
      Psc.append(t[1][0])
      td = list(1.0*array(d))
      for tdj in range(len(td)):
        if(td[tdj]!=0):
          td[tdj]=-td[tdj]
#        t = getDynPs(X,td,nMax=nMax)
      rds.append(td)
#      rdsS.append('c['+','.join(['%i'%di for di in td])+']')
      rdsS.append('_-_p%i_%s'%(dj+1,ParNames[dj]))
      Ps.append(t[0][1])
      Psc.append(t[1][1])
 

#    p,pc = getNConstPs(X,nMax=nMax,NTotal=NTotal)
#    rdsS.append('const')
#    Ps.append(p[1])
#    Psc.append(pc[1])
#    rdsS.append('Nconst')
#    Ps.append(p[0])
#    Psc.append(pc[0])


    wb=Workbook("SFwork/raw/ep_StatSign_DynCv1_M105_"+fname+"_Neq-"+NTotalNames[Nj]+"_nM%i"%nMax+".xlsx")


    red_format = wb.add_format({'bg_color':   '#FFC7CE',
                                'font_color': '#9C0006'})
    green_format = wb.add_format({'bg_color':   '#C7FFCE',
                                  'font_color': '#009C06'})
    def save(X,PNames,Times,sheetname,ftype=0):
    #    trint(sheetname)
    #    trint(shape(X))
        s = wb.add_worksheet(sheetname)
        s.write(0,0,'ProtNames\time')

        for tj in range(len(Times)):
            s.write(0,tj+1,Times[tj])

        for pj in range(len(PNames)):
            s.write(pj+1,0,PNames[pj])
            for tj in range(len(Times)):
                s.write(pj+1,tj+1,X[tj][pj])
        if(ftype==1):

          s.conditional_format(1,1,len(X[0])-1,len(X), {'type':     'cell',
                                        'criteria': '==',
                                        'value':     1,
                                        'format':    green_format})
        elif(ftype==2):
          s.conditional_format(1,1,len(X[0])-1,len(X), {'type': '3_color_scale',
                                         'min_color': "#00FF00",
                                         'mid_color': "#FBFF80",
                                         'max_color': "#FFFFFF",
                                         'min_type' : 'num',
                                         'mid_type' : 'num',
                                         'max_type' : 'num',
                                         'min_value': '0.045',
                                         'mid_value': '0.052',
                                         'max_value': '0.25',
                                          })

    trint('saving..')
    save(meanData,uProtNames,range(1,shape(meanData)[0]+1),"mean")
    save([list(a) + [min(a)] for a in Ps],uProtNames+['min p'],rdsS,"p",ftype=2)
    save([list(a) + [min(a)] for a in Psc],uProtNames+['min p'],rdsS,"pc",ftype=2)
    save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.05)],uProtNames+['_Sum'],rdsS,"p_0.05",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.01)],uProtNames+['_Sum'],rdsS,"p_0.01",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.25)],uProtNames+['_Sum'],rdsS,"pc_0.25",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.1)],uProtNames+['_Sum'],rdsS,"pc_0.1",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.05)],uProtNames+['_Sum'],rdsS,"pc_0.05",ftype=1)
    save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.01)],uProtNames+['_Sum'],rdsS,"pc_0.01",ftype=1)
    wb.close()
    trint('done.')

  """ """
  #