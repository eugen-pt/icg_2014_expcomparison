# -*- coding: utf-8 -*-
"""
Created on Tue Oct 14 13:59:18 2014

@author: root
"""

#preliminary data loading:

import random

from ep_bioinfo_2014_10_13b_expComp_loadData import *

fpath = 'SFwork/SpaceFlight_1_10.xls'
fpath = 'SFwork/HB.xls'
expnames=[]
#expnames = ['CF','HB','PE']
expnames = expnames + ['SpaceFlight_1_10']
#expnames = expnames + ['backup_men_1_6']

""" Loading individual data """

_uProtNames = {}
_uData = {}
for expname in expnames:
    ExpSubjNums,uTimes,_uProtNames[expname],_uData[expname]= \
                            loadExpCompStandData('SFwork/%s.xls'%expname,1)
""" Combining ProtNames and Data """                

uProtNames = []
[uProtNames.extend(_uProtNames[k]) for k in _uProtNames]
uProtNames_iD = {uProtNames[j]:j for j in range(len(uProtNames))}

## General characteristics         
#                            
NP = len(uProtNames)
# N(times*subjects) -- overall number of points in this script
NX = sum([prod(shape(_uData[k])[0:2]) for k in _uData])

uData = np.zeros((NX,NP))
uExpNames = []
xj=-1
for expname in expnames:
    for sj in range(shape(_uData[expname])[0]):
        for tj in range(shape(_uData[expname])[1]):
            xj=xj+1
            for pj in range(len(_uProtNames[expname])):
                pji = uProtNames_iD[_uProtNames[expname][pj]]
                uExpNames.append(expname)
                uData[xj,pj] = _uData[expname][sj][tj][pj]
  
sData = sum(uData,axis=1)
""" Histogram output """                
                
if(0):
    figure();
    hist(sData)                
    xlabel('N proteins present')
    ylabel('N timepoints')    
    title('Histogrm of different proteins counts present\n'+' , '.join(expnames))
    
""" some test random stuff.. """    
    
if(0):
    x = 1*(rand(1000,shape(uData)[1])<(mean(sData)/shape(uData)[1]))    
    xt = sum(x,axis=1)
    
    hist([sData,xt],normed=1)    

""" """
# Func to generate a simulated dataset 
#   of proteins present or absent based on a random distribution
#    
# distr's:
#
#   n -> Normal (N proteins = NP_mean+-NP_d)
#   u -> Uniform (N proteins in [ NP_mean-NP_d , NP_mean + NP_d ] )
#   r -> Random ( probability of a protein = NP_mean/N, NP_d unused)    
def genRandSet(NX=10,N=1000,distr='r',NP_mean=50,NP_d=15):
    if(distr in ['n','u']):
        R=zeros((NX,N))
        for xj in range(NX):
            if(distr=='u'):
                hnp = random.randint(int(NP_mean-NP_d),int(NP_mean+NP_d))
            elif(distr=='n'):
                hnp = int(randn()*NP_d+NP_mean)
            hnp = min(max(hnp,0),N-1)    
            R[xj,random.sample(range(N),hnp)]=1
    elif(distr=='r'):
        R = 1*(rand(NX,N)<(NP_mean*1.0/N))
    return R[:,sum(R,axis=0)>0]


# Func to get target distributions from data
#   namely - distr of number of timepoint a protein was present
def getDistr(Data):
    # number of points each protein is present
    S = sum(Data,axis=0)
    # histogram of the results <=> distribution
    return np.histogram(S,arange(0.5,shape(Data)[0]+0.6),normed=1)[0]
    
    
# This func generates distribution of distributions =) 
def getRandDistr(NX=10,N=1000,distr='r',NP_mean=50,NP_d=15,NRT=100):
    # random sets of experimental data
    X = [genRandSet(NX=NX,N=N,distr=distr,NP_mean=NP_mean,NP_d=NP_d) \
                                                        for j in range(NRT)]
    # corresponding dis'SpaceFlight_1_10'tributions
    H = [getDistr(x) for x in X]
    # resulting distribution of distributions -- only parameters (mean and sd)  
    return [mean(H,axis=0),std(H,axis=0)]

def genDistrLike(uData,NRT=100,N=-1,distr='r'):
    if(N<0):
        N = shape(uData)[1]
        
    sData = sum(uData,axis=1)
    return getRandDistr(NX = shape(uData)[0],N=N,distr=distr,NP_mean=mean(sData),NP_d = std(sData),NRT=NRT)

def estimate_N(N,uData,distr='r',NRT=100,use_sd=1):
    R=genDistrLike(uData,NRT=NRT,N=N,distr=distr)
    sim_H_mean = R[0]
    sim_H_std = R[1]
    expH = getDistr(uData)
    
    if(use_sd):
        return sqrt(mean([x*x for x in (expH-sim_H_mean)/(sim_H_std+0.00001)]))
    else:
        return sqrt(mean([x*x for x in (expH-sim_H_mean)]))
      
use_sd = 1

adds = ' wsd ' if use_sd else ''
      
test_N = range(100,2000,100)
test_N = range(100,30000,1000)

## backup_men_1_6
#test_N = range(50,400,20)
#
## SpaceFlight_1_10
#test_N = range(50,400,20)

x0=[estimate_N(j,uData,distr='u',NRT=100,use_sd=use_sd) for j in test_N]
figure('estimate_N'+adds+''+adds+'')
plot(test_N,x0)

figure('estimate_N'+adds+' - worst')
j = x0.index(max(x0))
xs = range(1,shape(uData)[0]+1)
plot(xs,getDistr(uData),'-b')
R = genDistrLike(uData,NRT=100,N=test_N[j],distr='u')
plot(xs,R[0],'-r')
plot(xs,R[0]+R[1],'--k',xs,R[0]-R[1],'--k')
legend(['Experimental','Simulated, mean','Sim+-SD'])
title('worst case: N = %i , error = %g'%(test_N[j],x0[j]))

figure('estimate_N'+adds+' - best')
j=x0.index(min(x0))
xs = range(1,shape(uData)[0]+1)
plot(xs,getDistr(uData),'-b')
R = genDistrLike(uData,NRT=100,N=test_N[j],distr='u')
plot(xs,R[0],'-r')
plot(xs,R[0]+R[1],'--k',xs,R[0]-R[1],'--k')
legend(['Experimental','Simulated, mean','Sim+-SD'])
title('Best case: N = %i , error = %g'%(test_N[j],x0[j]))
      
aaa

x=getRandDistr(shape(uData)[0],shape(uData)[1],distr='u',NP_mean=mean(sData),NP_d = std(sData))
""" """
""" """
""" """
""" plotting some distributions.. """
if(0):
    X=[genRandSet(shape(uData)[0]*100,shape(uData)[1],distr=s,NP_mean=mean(sData),NP_d = std(sData)) for s in ['n','u','r']]+[uData]
    if(0): #these distributions are quite easy to test
        T=[sum(x,axis=1) for x in X ]
        figure()
        hist(T,normed=1)
        legend(['n','u','r','Real'])
    D=[sum(x,axis=0)*1.0/shape(x)[0] for x in X ]
    figure()
    hist(D,normed=1)
    legend(['n','u','r','Real'])
""" """


""" """
""" """
""" """
#