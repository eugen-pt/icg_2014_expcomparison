# -*- coding: utf-8 -*-
"""
Created on Wed Oct 08 19:14:40 2014


requires ep_bioinfo_load105_alltimes to have been lauched

@author: ep_work
"""

from ep_bioinfo_load105 import *

from ep_bioinfo_2014_10_08a_ps_of_prots import *

PS = np.zeros([shape(PD)[1],shape(PD)[2]])
mD = np.zeros(shape(PS))
PSc = np.zeros(shape(PS))

for tj in range(24):
  six = ~isnan(PD[:,0,tj])

  mD[:,tj] = mean(PD[six,:,tj],axis=0) #debugging!  =) works
  PS[:,tj],PSc[:,tj] = getPs(PD[six,:,tj])


import xlwt

book = xlwt.Workbook(encoding="utf-8")

def write(ProtNames,X,sheet_name):

  S = book.add_sheet(sheet_name)

  S.write(0,0,"IPI\weekN")
  for wj in range(shape(PD)[2]):
    S.write(0,wj+1,weekColNames[wj])

  for pj in range(len(ProtNames)):
    S.write(pj+1,0,ProtNames[pj])
    for wj in range(shape(PD)[2]):
      S.write(pj+1,wj+1,X[pj,wj])

write(ProtNames,mD,"mean")
write(ProtNames,PS,"p_values")
write(ProtNames,PSc,"p_values_BenjHoch_FDRadj")

write(ProtNames,1*(PS<0.05),"p_lt_0.05")
write(ProtNames,1*(PSc<0.05),"FDRp_lt_0.05")


book.save("aaa.xls")


""" """
#

