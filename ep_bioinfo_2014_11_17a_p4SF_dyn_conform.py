import xlrd
from time import clock

tStart = clock()


def trint(s):
  print('%5.2f\t%s'%(clock()-tStart,s))

fname= 'backup_men_1_6'
ds = [[1,0],[0,1],[1,1],[-1,1]]
fname= 'SpaceFlight_1_10'
ds = [[1,0,0],[0,1,0],[0,0,1],[1,1,1],[-1,1,0],[-1,0,1],[0,-1,1],[-1,2,-1],[-2,1,1],[1,1,-2]]


def doStuff:
rb = xlrd.open_workbook('SFwork/'+fname+'.xls')

# sf 10subj =>

nMax=21


ExpData = []
ExpProtNames = []
ExpSubjNums = []
ExpTimes = []

try:
  subj_list
  subj_lists = 'subjs'+''.join(map(str,subj_list))
except:
  subj_list = range(len(rb.sheet_names()))
  subj_lists=''
for j in subj_list:
    if(str(j+1) in rb.sheet_names()):
#        trint('subj %i'%(j+1))
        ExpSubjNums.append(j+1)
        sheet = rb.sheet_by_name(str(j+1))

        ExpTimes.append(sheet.row_values(0,1))
        ExpProtNames.append(sheet.col_values(0,1))

        tData=[]
        for i in range(len(ExpTimes[-1])):
            tData.append(sheet.col_values(i+1,1))
        ExpData.append(tData)

try:
  upn
  uProtNames = upn
  usedAll=1
except:
  uProtNames = []
  for A in ExpProtNames:
      for s in A:
          if(s not in uProtNames):
              uProtNames.append(s)

  uProtNames.sort()
  usedAll=0

uPND = {}
for j in range(len(uProtNames)):
    uPND[uProtNames[j]]=j

uNP = len(uProtNames)


uData = []
for sj in range(len(ExpSubjNums)):
    sData = []
    for tj in range(len(ExpTimes[sj])):
        sData.append([0]*uNP)
        for pj in range(len(ExpProtNames[sj])):
            sData[-1][uPND[ExpProtNames[sj][pj]]] = ExpData[sj][tj][pj]
    uData.append(sData)

trint('loaded')

X = np.swapaxes(uData,0,1)

#aaa

resData = []
rescData = []
meanData = mean(uData,axis=0)

from ep_bioinfo_2014_11_13a_ps_of_prots_dyns import *


rds = []
Ps = []
Psc = []
for dj in range(len(ds)):
  trint('dyn %i/%i'%(dj,len(ds)))
  d=ds[dj]
  t = getDynPs(X,d,nMax=nMax)
  rds.append(d)
  Ps.append(t[0][0])
  Psc.append(t[1][0])
  td = list(-1.0*array(d))
  t = getDynPs(X,td,nMax=nMax)
  rds.append(td)
  Ps.append(t[0][0])
  Psc.append(t[1][0])



from xlsxwriter.workbook import Workbook
wb=Workbook("SFwork/raw/ep_StatSign_Dynv0_"+str(usedAll)+"_"+fname+".xlsx")


red_format = wb.add_format({'bg_color':   '#FFC7CE',
                            'font_color': '#9C0006'})
green_format = wb.add_format({'bg_color':   '#C7FFCE',
                              'font_color': '#009C06'})
def save(X,PNames,Times,sheetname,ftype=0):
#    trint(sheetname)
#    trint(shape(X))
    s = wb.add_worksheet(sheetname)
    s.write(0,0,'ProtNames\time')

    for tj in range(len(Times)):
        s.write(0,tj+1,Times[tj])

    for pj in range(len(PNames)):
        s.write(pj+1,0,PNames[pj])
        for tj in range(len(Times)):
            s.write(pj+1,tj+1,X[tj][pj])
    if(ftype==1):

      s.conditional_format(1,1,len(X[0])-1,len(X), {'type':     'cell',
                                    'criteria': '==',
                                    'value':     1,
                                    'format':    green_format})
    elif(ftype==2):
      s.conditional_format(1,1,len(X[0])-1,len(X), {'type': '3_color_scale',
                                     'min_color': "#00FF00",
                                     'mid_color': "#FBFF80",
                                     'max_color': "#FFFFFF",
                                     'min_type' : 'num',
                                     'mid_type' : 'num',
                                     'max_type' : 'num',
                                     'min_value': '0.05',
                                     'mid_value': '0.1',
                                     'max_value': '0.25',
                                      })

trint('saving..')
save(meanData,uProtNames,ExpTimes[0],"mean")
save([list(a) + [min(a)] for a in Ps],uProtNames+['min p'],[ '['+','.join(map(str,a))+']' for a in rds],"p",ftype=2)
save([list(a) + [min(a)] for a in Psc],uProtNames+['min p'],[ '['+','.join(map(str,a))+']' for a in rds],"pc",ftype=2)
save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.05)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"p_0.05",ftype=1)
save([list(a) + [sum(a)] for a in 1*(array(Ps)<0.01)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"p_0.01",ftype=1)
save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.05)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"pc_0.05",ftype=1)
save([list(a) + [sum(a)] for a in 1*(array(Psc)<0.01)],uProtNames+['_Sum'],[ '['+','.join(map(str,a))+']' for a in rds],"pc_0.01",ftype=1)
wb.close()
trint('done.')

""" """
#