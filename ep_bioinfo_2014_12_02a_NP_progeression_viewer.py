# -*- coding: utf-8 -*-
"""
Created on Tue Dec 02 15:42:17 2014

@author: ep_work
"""


doover = 0


randover = doover or 0


if(doover):
  try:
    del uExpAData
  except:
    pass

try:
  uExpAData
except:
  from ep_bioinfo_2014_10_08b_load_ExpComp_Data import *





tx=sum(cumsum(uD,axis=0)>0,axis=1)

if(randover):
  try:
    del mtx
  except:
    pass

try:
  mtx
except:
  txs = []
  import random
  print('random testing..')
  for rj in range(1000):
    tix = random.sample(range(shape(uD)[0]),shape(uD)[0])
    tx=sum(cumsum(uD[tix,:],axis=0)>0,axis=1)
    txs.append(tx)
  print('done.')
mtx = mean(txs,axis=0)
stx = std(txs,axis=0)
t = range(1,len(mtx)+1)
#figure()
#plot(sum(uD,axis=1))
#plot(t,mtx,'-',t,mtx+stx,'--',t,mtx-stx,'--')

nsY0 = stx/max(mtx)
nY0 = mtx/max(mtx)
nt = array(t)*1.0/max(t)


def dnpars(p):
  p0=p[0]
  k0=p[1]*0.0001
  k1=p[2]*0.01
  return (p0,k0,k1)

def DoY(p0,k0=-153,k1=-153):
  try:
    k0
    k1
    if(k0==-153):
      raise ValueError()
  except:
    p0,k0,k1=dnpars(p0)
  return p0*(1-k0**nt)+(1-p0)*(1-k1**nt)

def R2(p):
  p0,k0,k1 = dnpars(p)
  y = DoY(p0,k0,k1)
  j = range(len(nY0)-1)
  return sqrt(mean(((nY0[j]-y[j])/nsY0[j])**2))

from scipy.optimize import *
method = 'L-BFGS-B'
method = 'COBYLA'
method = 'SLSQP'
r = minimize(R2,[1,1,1],method=method,bounds=[(0.1,1.9),(0.001,100),[0.01,100]])


p0=0.5
k0 = 0.00001
k1 = 0.01
figure('Stuff')
plot(nt,nY0,'-k',nt,nY0+nsY0,'--k',nt,nY0-nsY0,'--k',nt,DoY(p0,k0,k1),'-b',nt,DoY(r.x),'-r')


""" """
#

