# -*- coding: utf-8 -*-
"""
Created on Mon May 16 12:00:56 2016

@author: ep
"""

"""      Imports      """

from ep_bioinfo_loadBGO import loadBGO

try:
    uExpAData
    uProtNames
    ExpNames
    ExpSubjNums
    uNP
except:
    from ep_bioinfo_2014_10_08b_load_ExpComp_Data import *

"""      Consts       """

try:
    BGO
except:
    BGO = loadBGO('uProtNames_p0.bgo')

""" Local Definitions """

def LoadData(key):
    tix = [k  for k in range(len(ExpNames)) if key in ExpNames[k]]

    Data = [uExpAData[k] for k in tix]
    hExpSubjNums = [ExpSubjNums[k] for k in tix]
    hExpNames = [ExpNames[k] for k in tix]
    hExpATimes = [ExpATimes[k] for k in tix]
    huExpAData = [uExpAData[k] for k in tix]


    Times = []
    for a in hExpATimes:
        for aa in a:
            Times.extend(aa)
    Times = list(set(Times))
    Times.sort()


    Subjs = []
    for a in hExpSubjNums:
        Subjs.extend(a)
    Subjs = list(set(Subjs))
    Subjs.sort()
    Subjs_iD = {Subjs[j]:j for j in range(len(Subjs))}

    Data = np.zeros((len(Subjs),len(Times),uNP,))*nan
    for expj in range(len(hExpATimes)):
        for subjj in range(len(hExpSubjNums[expj])):
            subjN = hExpSubjNums[expj][subjj]
            for ti in range(len(Times)):
                htime = Times[ti]
                if(htime in hExpATimes[expj][subjj]):
                    tj = hExpATimes[expj][subjj].index(htime)
                    Data[Subjs_iD[subjN],ti,:] = huExpAData[expj][subjj][tj]
    return {'Data':Data,'Times':Times,'Subjs':Subjs,'uProtNames':uProtNames,'uProtNames_iD':{uProtNames[j]:j for j in range(len(uProtNames))}}

def meanProtDyn(_R,Prot):
    ix = _R['uProtNames_iD'][Prot]

    tData = _R['Data'][:,:,ix]*1.0
    tData_nn = tData*1
    tData_nn[isnan(tData_nn)]=0
    return sum(tData_nn,axis=0)/sum(~isnan(tData),axis=0)


"""   Data Loading    """

R = LoadData('SpaceFlight')
R_bu = LoadData('backup_men')

"""    Processing     """

BGO_GO_NProts = {g:len(BGO['GO_Prots'][g]) for g in BGO['GO_Prots']}

GO_dyns = {}
for goid in BGO_GO_NProts:
    if(BGO_GO_NProts[goid]>50):
        GO_dyns[goid] = zeros((len(R['Times']),))
        for p in BGO['GO_Prots'][goid]:
            GO_dyns[goid] = GO_dyns[goid]+meanProtDyn(R,p)*1.0/BGO_GO_NProts[goid]

GO_dyns_goids = GO_dyns.keys()
GO_dyns = array([GO_dyns[k] for k in GO_dyns])
GO_dyns_m = mean(GO_dyns,axis=1)
GO_dyns = GO_dyns/GO_dyns_m
"""      Saving       """



""" """
#
