# -*- coding: utf-8 -*-
"""
Created on Mon May 16 11:26:33 2016

@author: ep
"""

"""      Imports      """


"""      Consts       """



""" Local Definitions """

def loadBGO(fpath):
    LS = open(fpath).readlines()

    R={}
    R['Prots_all'] = []
    lastJ=0
    for j in range(len(LS)):
        if('The selected cluster :' in LS[j]):
            for i in range(j+1,len(LS)):
                if(len(LS[i])>1):
                    R['Prots_all'].extend(LS[i].replace('\n','').split('\t'))
                else:
                    lastJ=i
                    break
    R['Prots_undone'] = []
    for j in range(lastJ,len(LS)):
        if('No annotations were retrieved for the following entities:' in LS[j]):
            for i in range(j+1,len(LS)):
                if(len(LS[i])>1):
                    R['Prots_undone'].extend(LS[i].replace('\n','').split('\t'))
                else:
                    lastJ=i
                    break

    header = LS[lastJ+1].split('\t')
    header[-1]='Prots'

    header_iD = {header[j]:j for j in range(len(header))}

    R['GO_raw'] = {h:[] for h in header}
    for j in range(lastJ+1,len(LS)):
        L = LS[j].replace('\n','').split('\t')
        for i in range(len(header)-1):
            R['GO_raw'][header[i]].append(L[i])
        R['GO_raw'][header[-1]].append(L[-1].split('|'))

    for h in ['Description','Prots']:
        R['GO_'+h] = {R['GO_raw']['GO-ID'][j]:R['GO_raw'][h][j] for j in range(len(R['GO_raw'][h]))}

    R['Prots'] = []
    for a in R['GO_raw']['Prots']:
        R['Prots'].extend(a)
    R['Prots'] = list(set((R['Prots'])))

    R['Prots_GOids'] = {s:[] for s in R['Prots']}

    for goid in R['GO_Prots']:
        for g in R['GO_Prots'][goid]:
            R['Prots_GOids'][g].append(goid)

    return R
"""   Data Loading    """

if(__file__=='__main__'):
    R=loadBGO('uProtNames_p0.bgo')

"""    Processing     """


"""      Saving       """



""" """
#
